#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 10:33:45 2017

Some utilities of questionable usability (some may be reinventions of a wheel)

@author: nle003
"""

import pickle
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import io
import scipy.constants as phys
import scipy.special as spec
import scipy.interpolate

####################################################################################################
#%% A few general-purpose routines

def line_interp2d(x,y,xp,yp,zp):
    """Workaround around clumsy 2D interpolation in scipy. Stolen from
    https://stackoverflow.com/questions/47087109/
    evaluate-the-output-from-scipy-2d-interpolation-along-a-curve"""
    shape=x.shape
    assert y.shape==shape
    f = scipy.interpolate.interp2d(xp, yp, zp, bounds_error=True)
    return scipy.interpolate.dfitpack.bispeu(
            f.tck[0], f.tck[1], f.tck[2], f.tck[3], f.tck[4],
            x.flat, y.flat)[0].reshape(shape)

def ones_as(x):
    "Scalar or vector ones"
    return np.ones(np.asarray(x).shape)[()]

def zeros_as(x):
    "Scalar or vector zeros"
    return np.zeros(np.asarray(x).shape)[()]

def anti_diff(arr0,darr):
    "The inverse operation to np.diff for 1D arrays"
    return np.cumsum(np.hstack((arr0,darr)))

def ave(x):
    """Useful for plotting histograms:
        plt.plot(ave(edges),hist,'.')
    See also: duplicate, meandrize
    """
    return (x[1:]+x[:-1])/2

# NOTE: these two function are not really needed, use drawstyle='steps-post' keyword to plot
def duplicate(x):
    """Useful for plotting histograms:
        plt.plot(meandrize(edges),duplicate(hist))
    """
    return np.tile(np.array([x]).T,2).flatten()

def meandrize(x):
    """Useful for plotting histograms:
        plt.plot(meandrize(edges),duplicate(hist))
    """
    return np.hstack((x[0],duplicate(x[1:-1]),x[-1]))

def make_dispatch(arg,boundaries):
    "Auxiliary function to be used with numpy.choose"
    tmp=np.asarray(arg)
    res=np.zeros(tmp.shape,dtype=np.int)
    for b in boundaries:
        res += (tmp>=b).astype(np.int)
    return res

def dispatch(arg,boundaries,results):
    """Choose results depending on the arg belonging to an interval
    defined by boundaries"""
    return np.choose(make_dispatch(arg,boundaries),results)


def map_to_ndarray(func,ndarr,depth=1,params=(),keywords={}):
    """A more intuitive version of np.vectorize()"""
    if depth==0:
        return func(ndarr,*params,**keywords)
    elif depth>0:
        return np.array([map_to_ndarray(func,subarr,depth=depth-1,
                                        params=params,keywords=keywords)
                        for subarr in ndarr])
    else:
        raise Exception('depth')

def imports():
    "List all the imported modules"
    import types
    for name, val in globals().items():
        if isinstance(val, types.ModuleType):
            print(val.__name__,'as',name)

def reimport(module):
    """Does not always work. Please try built-in IPython commands, or (still better)
    restart the kernel"""
    import importlib
    for name in dir(module):
        if name[0:2]!='__': delattr(module, name)
    importlib.reload(module)

def fig_open(fname):
    with open(fname,'rb') as f:
        fig = pickle.load(f)
    fig.show()
    return fig

def goodplot():
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
    plt.gca().get_yaxis().get_major_formatter().set_useOffset(False)
    plt.draw()

####################################################################################################
#%% Terminal colors and pretty printing
# See https://www.digitalocean.com/community/tutorials/how-to-customize-your-bash-prompt-on-a-linux-vps
# \e -- same as \033 is escape char
# \e[ -- start of color info
# m\] -- end of color info
# Color info: 0,1,4 - normal,bold or underlined text; 3x -- foreground color; 4x -- background color, where
# x = 0-7 is black, red, green, yellow, blue, purple, cyan, white
# \[\e]0; -- start of the terminal title (not displayed in the normal prompt)
# \a\] -- end of the terminal title

__colors = {} # will not get visible imported by from utils import *
def set_colors(b=True):
    "Make a colorful terminal output. Use set_colors(False) to turn it off."
    global __colors
    if b:
        __colors = {'black':'\033[30m', 'red':'\033[31m', 'green':'\033[32m', 'yellow':'\033[33m',
                  'blue':'\033[34m', 'magenta':'\033[35m', 'cyan':'\033[36m', 'white':'\033[37m',
                  'end':'\033[0m', 'bold':'\033[1m', 'underline':'\033[4m'}
    else:
        __colors = {'black':'', 'red':'', 'green':'', 'yellow':'',
                  'blue':'', 'magenta':'', 'cyan':'', 'white':'',
                  'end':'', 'bold':'', 'underline':''}

set_colors()

def get_colors(arg):
    "May be unnecessary"
    return __colors[arg]

def WARNING():
    "Prints the word 'WARNING:' in red. See set_colors"
    # Always deferred evaluation, could just use colors['red'] etc
    return get_colors('red')+get_colors('bold')+'WARNING:'+get_colors('end')

def string(*a,**kw):
    "Python3-style print into a string"
    s=io.StringIO()
    kw.setdefault('file',s)
    kw.setdefault('end','')
    print(*a,**kw)
    tmp=s.getvalue()
    s.close()
    return tmp

class myfloat(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

def header(*a,textwidth=100,color=None,bold=False,fill='*',print_kw={}):
    s = string(*a,**print_kw)
    if len(s)==0:
        s = fill*textwidth
    else:
        if len(s)>textwidth-2:
            return s
        n1 = (textwidth-2-len(s))//2
        n2 = textwidth-2-len(s)-n1
        s = fill*n1 + ' ' + s + ' ' + fill*n2
    if color is not None:
        s = s + get_colors('end')
        if bold:
            s = get_colors('bold') + s
        s = get_colors(color) + s
    return s

def bigprint(*a,textwidth=100):
    print(get_colors('blue') + get_colors('bold') + '*'*textwidth)
    print(header(*a,textwidth=textwidth))
    print('*'*textwidth + get_colors('end'))
