#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  4 19:18:10 2016

Solution of advective equation:

df/dt + div(v*f) = 0

using various linear schemes.

f in div(v*f) is represented as a "face value" of f

Literature:

Leonard [1979] - 3rd order upwind scheme in 1D (called QUICKEST)
Leonard and Niknafs [1991] - in 2D (called UTOPIA)
Leonard [1991] - the flux-limiting algorithm in 1D (called ULTIMATE)

Previous versions:
    advection2d_rotation_symbolic.py
    advection2d_rotation_waverchoice.py
    advection2d_rotation_demo.py

@author: nle003
"""

#%% Preliminaries
# Imports
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from my_utils import myfloat
import time
import trump
trump.DEBUGLEVEL=0
trump.SET_SPARSE(True)
from trump import ExtendedArray2D, end, Grid, grid_x,\
    c_dif, n_dif, c_ave, n_ave, c_upc, n_upc, Field2D

from trump_2D import Geometry2D_car, AdvectionStepI, UpdateablePcolor
from trump_2D import c_difx, c_dify, n_difx, n_dify

def n_div(fx,fy): return n_dif(fx,0)+n_dif(fy,1)
def c_upx(f,v): return c_upc(f,v,0)
def c_upy(f,v): return c_upc(f,v,1)

################################################################################
#%% Testing
vmax = 0.45
do_opposite_rotation=False
num_smear = 0
do_log_plot = False

# Setup the grid and velocity field: CCW for r<R1, CW for R1<r<R2, w=vmax/R2
# The unknown function f is calculated on a Nx x Ny array, at x,y coordinates
Lx = 2; Ly = 2; dx=.01; dy=.01;
#Lx = 200; Ly=200; dx=1; dy=1
Nx=np.int(Lx/dx)
Ny=np.int(Ly/dy)
gridx = Grid(n_cells=Nx,delta=dx,start=-Lx/2)
gridy = Grid(n_cells=Ny,delta=dy,start=-Ly/2)
# Extended coordinates are for pcolor plots
#xe = geom_x(geomx,0)
#ye = geom_x(geomy,0)
#x=geom_x(geomx,1)
#y=geom_x(geomy,1)

# Velocity (scaled), calculated on the grid edges as v = curl (psi z)
#R2=np.min([Lx/2-3*dx,Ly/2-3*dy])
R2 = np.sqrt(2)*Lx/2
if do_opposite_rotation:
    R1 = np.min([Lx/4,Ly/4])
else:
    R1 = 0
vangle=vmax/R2 # angular velocity

# Zero-divergence velocity, curl of psi*z_unit
geom = Geometry2D_car(gridx,gridy,nls=(3,3),nus=(3,3))
psi=ExtendedArray2D(geom,stags=(1,1),nls=(3,3),nus=(3,3))
yg,xg=np.meshgrid(psi.xe(1),psi.xe(0))
rg=np.sqrt(xg**2+yg**2)
psi.arr = -rg**2/2*vangle # at points (i-1/2,j-1/2)
i2=(rg>=R1)
psi.arr[i2]=rg[i2]**2/2*vangle-R1**2*vangle
i3=(rg>=R2)
psi.arr[i3]=R2**2/2*vangle-R1**2*vangle
vx =  n_dify(psi) # vx at points (i-1/2,j)
vy = -n_difx(psi) # vy at points (i,j-1/2)
# Check if the divergence is zero at points (i,j)
print('max div v =',np.max(np.abs(n_div(vx,vy)).arr),flush=True)
dt = 1/(np.max(vx.arr)/dx+np.max(vy.arr)/dy)

################################################################################
#%% Prepare the figures
methods=['CIR','MacCormack','MacCormack_Reverse','UTOPIA','ULTIMATE']
nfig=len(methods)
savedirs=[None]*nfig
info = lambda n,f,m: '[{:.3f},{:.3f}],n={:.2f},{:s}'.format(np.min(f.arr),np.max(f.arr),n,m)
zmin,zmax = (-8,.5) if do_log_plot else (-.4,1.4)
fig=[UpdateablePcolor(100+kfig,figsize=(6,5),savedir=savedirs[kfig],cmap=mpl.cm.jet,zmin=zmin,zmax=zmax)
    for kfig in range(nfig)]

#%%#############################################################################
# Initial value
fini = ExtendedArray2D(geom,stags=(0,0),nls=(2,2),nus=(2,2))
fini.alloc(np.double)
yg,xg=np.meshgrid(fini.xe(1),fini.xe(0))
xsqc=0.; ysqc=Ly/6.
ax=Lx/4; ay=Ly/6
fini.arr[(np.abs(xg-xsqc)<ax) & (np.abs(yg-ysqc)<ay)]=1.
def smear(ea,n=0):
    for k in range(n):
        ea.setv = n_ave(c_ave(n_ave(c_ave(ea,0),0),1),1)
    #return ea
smear(fini,num_smear)

################################################################################
#%% The main cycle
# Initial value
farr=[]

for kfig in range(nfig):
    if kfig>=1: dd = 2
    else: dd = 1
    f0=Field2D('f'+str(kfig+1),geom,stags=(0,0),nls=(dd,dd),nus=(dd,dd))
    tt1 = time.time()
    if dd==1:
        f0.bc[-1,-1:end+1]=0
        f0.bc[end+1,-1:end+1]=0
        f0.bc[:,-1]=0
        f0.bc[:,end+1]=f0.bc[:,end]
    else:
        f0.bc[-2:-1,-2:end+2]=0
        f0.bc[end+1:end+2,-2:end+2]=0
        f0.bc[:,-2:-1]=0
        f0.bc[:,end+1]=f0.bc[:,end]
        f0.bc[:,end+2]=f0.bc[:,end]
    tt2 = time.time()
    f0.bc.freeze() # takes pretty long
    tt3 = time.time()
    print('To record: t = ',tt2-tt1,', to freeze = ',tt3-tt2,flush=True)
    farr.append(f0)
#ye = f0.pcolor_x(1)
for f0 in farr:
    f0.setv = fini
totals = [geom.integrate(f) for f in farr]
print('totals =',totals)#xe = f0.pcolor_x(0)
#%%
for kt in range(50001):
    for kfig in range(nfig):
        farr[kfig] += AdvectionStepI(farr[kfig],vx*dt,vy*dt,methods[kfig])
        farr[kfig].bc.apply()
    #f = farr[-1]
    #fv1, dfv3 = FaceValue_UTOPIA(f,vx*dt,vy*dt)
    #fluxx,fluxy = TVD_ULTIMATE(f,(vx*dt,vy*dt),fv1,dfv3,apply=True)
    #f -= geom.Div(fluxx,fluxy)
    #f.bc.apply()
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0:
        n_turns = vangle*dt*kt/(2*np.pi)
        for kfig in range(nfig):
            f = farr[kfig]
            dtot = geom.integrate(f)-totals[kfig]
            fplot = np.log10(f+1e-8) if do_log_plot else f
            fig[kfig].plot(fplot,title=info(n_turns,f,methods[kfig]+' '+str(myfloat(dtot))))
        plt.pause(0.1)
        # fig[0].wait() # progress by clicking mouse in Figure 100
