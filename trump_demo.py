#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 14:58:27 2016

We demonstrate (almost all) features of TRUMP by solving a discretized Poisson equation

    Δ(φ)=-ρ

for φ with given ρ, in 2D in cartesian coordinates. While doing this, we abuse
the feature of Python3 which allows Greek characters to be used in variable names.

@author: nle003
"""

import numpy as np
import matplotlib.pyplot as plt
import trump
from trump import end, Grid, Geometry2D, Field2D, Solver2D, c_dif, n_dif
from trump.extended_array import grid_stop

#%%
#trump.SET_SPARSE(True) # use sparse matrices for operators, this is default
gridx = Grid(n_cells=50,delta=.02,start=0.0)
gridy = Grid(n_cells=40,delta=.025,start=0.0)
geom = Geometry2D(gridx,gridy)
# 'stags' is the staggering flag of the grid. '0' means the values are defined
# at nodes, while '1' would mean that the values are defined at centers of cells.
# 'nls' and 'nus' is the number of ghost cell layers on the bottom (lower) and
# the top (upper) boundaries of the array.
ρ = Field2D('ρ',geom,stags=(0,0),nls=(-1,-1),nus=(-1,-1))
# The charge density ρ does not need to be defined at the boundary, so the
# negative number of ghost cells is specified (we cut off one layer)
print('Setting ρ=1 in the domain [',gridx.start,'--',grid_stop(gridx),'x',\
    gridy.start,'--',grid_stop(gridy),']')
ρ[1:end-1,1:end-1] = 1
# note that ρ[0,...] or ρ[end,...] whould give an error because the boundary is cut off.
# TRUMP uses MATLAB-style indexing, not Python-style! In particular, the upper
# boundary ('stop' of the slice) truly _is_ the last element, not the element after the last
# as it would be in Python indexing. Also, negative indices in ExtendedArrays
# mean exactly what they mean - ghost cells below the lower boundary.
φ = Field2D('φ',geom,stags=(0,0),nls=(0,0),nus=(0,0))
print('Setting φ=0 on the right edge, bottom and top')
φ.bc[end,:] = 0 # ':' is equivalent to '0:end'
φ.bc[1:end-1,0] = 0
# -- We cannot use φ.bc[:,0] = 0 here because it would conflict/redefine the previous BC
φ.bc[1:end-1,end] = 0
print('Setting φ=0 on the left edge')
φ.bc.record('left edge',is_variable=True)
φ.bc[0,:] = 0
φ.bc.freeze() # This is necessary - checks BC consistency and prepares BC for solvers
def Δ(f):
    "Laplacian"
    # c_dif acts on stag=0 ExtendedArray's ('central' difference), the result
    # is a stag=1 array. The opposite is for n_dif ('node' difference).
    return n_dif(c_dif(f,0),0) + n_dif(c_dif(f,1),1)
φ_solver = Solver2D(φ, -Δ(φ.symb)) # Poisson equation with a given RHS = -Δ(φ)
#%% Plot #1
print('Solving Δφ=-ρ with φ=0 on the left edge ..')
φ_solver.solve_full(φ,ρ) # BC are automatically included
print('Error =',np.max(np.abs(Δ(φ)+ρ).arr)) # Out: 5.00155472594e-13
xe = φ.pcolor_x(0); ye = φ.pcolor_x(1)
# xe, ye are just for 'pcolor', these are a not the points where the function is
# defined. To get the grid points, use φ.x(0) and φ.x(1)
# print('Please close the figure window to continue') # if not using IPython
plt.figure(1,figsize=(6,5))
plt.clf()
plt.pcolor(xe,ye,φ[:,:].T)
plt.gca().set_aspect('equal')
plt.xlabel('x'); plt.ylabel('y');
plt.title(r'$\phi$ for $\rho=1$ and $\phi=0$ at the boundary')
plt.colorbar()
plt.savefig('trump_fig_1.pdf')
#%% Plot #2
print('Updating the BC to φ=1 on the left edge')
φ.bc.update('left edge')
φ.bc[0,:] = 1
φ.bc.end_update() # optional
print('Solving Δφ=-ρ with φ=1 on the left edge ..')
φ_solver.solve_full(φ,ρ) # the solver already knows about the new BC
print('Error =',np.max(np.abs(Δ(φ)+ρ).arr)) # Out: 4.15134593368e-12
plt.figure(2,figsize=(6,5))
plt.clf()
plt.pcolor(xe,ye,φ[:,:].T)
plt.gca().set_aspect('equal')
plt.xlabel('x'); plt.ylabel('y');
plt.title(r'$\phi$ for $\rho=1$, $\phi=1$ at left edge')
plt.colorbar()
plt.savefig('trump_fig_2.pdf')

