#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 18 18:48:23 2017

Test of advective schemes in cylindrical coordinates.

The velocity profile we test is (such that div v = 0, vz=0 at z=+-L/2 and vr=0 at r=R):
    
    vr = vo (π/L) J_1(br/R) sin(πz/L) = -dψ/dz 
    vz = vo bJ_0(br/R) cos(πz/L) = (1/r)d(rψ)/dr

with b being the first root of J_1, b=spec.jn_zeros(1,1)[0]=3.8317

It may be represented as curl A where A_φ = ψ and is given by

    ψ(r,z) = vo J_1(br/R) cos(πz/L)


@author: nle003
"""

#%% Preliminaries
import time
from my_utils import np,plt,spec,mpl,myfloat
π = np.pi
b=spec.jn_zeros(1,1)[0]

import trump
trump.DEBUGLEVEL=2
from trump import end, span, Grid, ExtendedArray2D, Field2D, c_ave, n_dif
from trump_2D import Geometry2D_cyl, UpdateablePcolor, AdvectionStepI

#def c_avez(f): return c_ave(f,1)

#%% Geometry setup
vo = 1
R = 1
L = 2
Nr = 100
Nz = 200
dr = R/Nr
dz = L/Nz
gridr = Grid(n_cells=Nr,delta=dr,start=0)
gridz = Grid(n_cells=Nz,delta=dz,start=-L/2)
cyl = Geometry2D_cyl(gridr,gridz,nls=(2,2),nus=(2,2))

#%% Velocity
divergence_free=True
if divergence_free:
    # Zero-divergence velocity, curl of psi*phi_unit
    psi=ExtendedArray2D(cyl,stags=(1,1),nls=(3,3),nus=(3,3))
    zpg,rpg=np.meshgrid(psi.xe(1),psi.xe(0))
    if False:
        b=spec.jn_zeros(1,1)[0]
        psi.arr=vo*spec.j1(b*rpg/R)*np.cos(π*zpg/L)
    else:
        # Psi can actually be arbitrary, just make sure vr=0 at r=0
        psi.arr = vo*np.sin(π*rpg/R)*np.cos(π*zpg/L)
    vr = -n_dif(psi,1) # vx at points (i-1/2,j)
    vz =  n_dif(psi*c_ave(cyl.rd,1),0)/c_ave(cyl.ri,1) # vy at points (i,j-1/2)
else:
    nls_r=cyl.r.nls
    vr = ExtendedArray2D(cyl,stags=(1,0),nls=cyl.rd.nls,nus=cyl.ri.nus)
    zvg,rvg = np.meshgrid(vr.xe(1),vr.xe(0))
    vr.arr = vo*np.sin(π*rvg/R)*np.sin(π*zvg/L)
    vz = ExtendedArray2D(cyl,stags=(0,1),nls=cyl.ri.nls,nus=cyl.ri.nus)
    zvg,rvg = np.meshgrid(vz.xe(1),vz.xe(0))
    vz.arr = vo*np.cos(π*rvg/R)*np.cos(π*zvg/L)
    
# Check if the divergence is zero at points (i,j)
print('max div v =',np.max(np.abs(cyl.Div(vr,vz)).arr),flush=True)

dt = min(dr,dz)/max(np.max(vr.arr),np.max(vz.arr))

#%% Initial value
methods = ['CIR','MacCormack','MacCormack_Reverse','UTOPIA','ULTIMATE']
nfig = len(methods)
fs=[]
totals = np.zeros((nfig,))
if False:
    rtmp = cyl.ri.copy()
    rtmp.arr=cyl.ri.arr.copy()
    rtmp[0,span]=dr/8
    def get_tot(f):
        return 2*np.pi*dz*dr*np.sum((f*rtmp)[:,:])
else:
    def get_tot(f):
        return cyl.integrate(f)
for kfig in range(nfig):
    if methods[kfig] in ['CIR']:
        dd=1
    elif methods[kfig] in ['MacCormack','MacCormack_Reverse','UTOPIA','ULTIMATE']:
        dd=2
    else:
        raise Exception('Unknown method')
    f0 = Field2D('f'+str(kfig+1),cyl,stags=(0,0),nls=(dd+1,dd),nus=(dd,dd))
    tt1 = time.time()
    for k in range(dd+1):
        f0.bc[-k-1,:end-1]=f0.bc[k+1,:end-1] # The values at nz=end are dependent due to periodic bc
        f0.bc[:,end+k]=f0.bc[:,k]
    for k in range(1,dd+1):
        f0.bc[:,-k]=f0.bc[:,end-k]
    tt2 = time.time()
    assert f0.bc.num>0 # we have set at least some boundary conditions
    f0.bc.freeze() # takes pretty long
    tt3 = time.time()
    print('To record: t = ',tt2-tt1,', to freeze = ',tt3-tt2,flush=True)
    zfg,rfg=np.meshgrid(f0.xe(1),f0.xe(0))
    zc=0;
    ar=2*R/3; az=L/4
    f0.arr[(np.abs(zfg-zc)<az) & (np.abs(rfg)<ar)]=1.
    totals[kfig]=get_tot(f0)
    fs.append(f0)
print('totals =',totals)

#%%
fig = [UpdateablePcolor(100+k,figsize=(6,5),zmin=-0.4,zmax=1.4,cmap=mpl.cm.jet)
    for k in range(nfig)]
info = lambda k,f,m: '[{:.3g},{:.3g}],k={},{:s}'.format(np.min(f.arr),np.max(f.arr),k,m)
rc,zc = cyl.xcs
#%%
for kt in range(201):
    for kfig in range(nfig):
        f = fs[kfig]
        #tmp = cyl.AdvectionStepI(f,vr*dt,vz*dt,methods[kfig])
        #print(get_tot(tmp))
        f += AdvectionStepI(f,vr*dt,vz*dt,methods[kfig])
        #f[f<0]=0
        f.bc.apply()
        #dtot = cyl.integrate(f,rb=np.array([0,rc[end]]),zb=np.array([zc[0],zc[end]]))-totals[kfig]
        dtot = get_tot(f)-totals[kfig]
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0:
        for kfig in range(nfig):
            fig[kfig].plot(fs[kfig],title=info(kt,fs[kfig],methods[kfig]+' '+str(myfloat(dtot))))
        plt.pause(0.1)
        # fig[0].wait() # progress by clicking mouse in Figure 100
