#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 16:45:54 2016

A customized NumPy ndarray, in order to make operations like v * f and
v + f work with ndarray v and arbitrary class f.

NOTE: f * v and f + v work fine even without it

@author: nle003
"""

import numpy as np
import numbers

class custom_ndarray(np.ndarray):
    overloaded_types=[]
    def __mul__(self,obj):
        for type_ in self.__class__.overloaded_types:
            if isinstance(obj,type_): return obj.__rmul__(self)
        return super().__mul__(obj)
    def __add__(self,obj):
        for type_ in self.__class__.overloaded_types:
            if isinstance(obj,type_): return obj.__radd__(self)
        return super().__add__(obj)
    def __sub__(self,obj):
        for type_ in self.__class__.overloaded_types:
            if isinstance(obj,type_): return obj.__rsub__(self)
        return super().__sub__(obj)

def reset_overloaded_types():
    custom_ndarray.overloaded_types = []

# old version
def overload_for_v1(type_):
    if not type_ in custom_ndarray.overloaded_types:
        custom_ndarray.overloaded_types.append(type_)

def overload_for(type_):
    # If there is already with this name, replace
    names = list(map(lambda t: getattr(t,'__name__'),custom_ndarray.overloaded_types))
    try:
        ii = names.index(type_.__name__)
    except ValueError:
        print('Registering custom operations ( ndarray %',type_.__name__,')')
        custom_ndarray.overloaded_types.append(type_)
    else:
        print('Re-registering custom operations ( ndarray %',type_.__name__,')')
        custom_ndarray.overloaded_types[ii]=type_
        
# old version
def customize_v1(arr):
    tmp=custom_ndarray(arr.shape,dtype=arr.dtype)
    tmp[...]=arr
    return tmp

#%% An optimized version
def customize(arr):
    if isinstance(arr,custom_ndarray):
        return arr
    elif isinstance(arr,np.number):
        return np.asscalar(arr)
    elif isinstance(arr,numbers.Number):
        return arr
    else:
        return arr.view(custom_ndarray)

def decustomize(carr):
    if isinstance(carr,custom_ndarray):
        #tmp=np.ndarray(carr.shape,dtype=carr.dtype)
        #tmp[...]=carr
        #return tmp
        return np.array(carr) # subok = False
    else:
        return carr

if __name__=='__main__':
    #%% Create test classes
    class R:
        def __repr__(self):
            "The most generic __repr__"
            return '%s(%s)' % (self.__class__.__name__, \
                ', '.join('%s=%s' % (k,repr(self.__dict__[k])) \
                                       for k in sorted(self.__dict__)))
    class A(R): pass
    class B(R): pass
    class C(R): pass
    class D(R): pass
    class E(R): pass

    #%% Create multiplication operation for class A with results in B,C,D,E
    import numbers
    def Amul(t1,t2):
        if isinstance(t2,numbers.Number): return B()
        else: return C()
    A.__mul__ = Amul
    def Armul(t1,t2):
        if isinstance(t2,numbers.Number): return D()
        else: return E()
    A.__rmul__ = Armul
    overload_for(A)
    
    #%% Demo for numbers
    a = A()
    print('2*a =',2*a)
    print('a*2 =',a*2)

    #%% Test these classes
    vo = np.arange(1,6,dtype=np.float)
    print('vo is a',vo.__class__.__name__)
    print('a*vo =',a*vo) # gives C()
    print('vo*a =',vo*a) # gives an array of D()'s
    print('vo[0]*a =',vo[0]*a)
    print('c(vo[0])*a =',customize(vo[0])*a)
    v=customize(vo)
    print('v is a',v.__class__.__name__)
    print('a*v =',a*v) # gives C()
    print('v*a =',v*a) # gives E(), not array of D()'s as with NumPy ndarray
   
    #%% All normal operations should still work
    print('v*v =',repr(v*v))

    #%% Back to normal
    print('v =',repr(v))
    print('vo =',repr(vo),'=',repr(decustomize(v)))
    
    #%% Acting on classes supporting subscripting fails for certain types
    class F(R): pass
    class Indexable(R):
        def __init__(self,l=1):
            self.l=l
        def __getitem__(self,k):
            if k<self.l and k>=-self.l:
                return self
            else:
                raise IndexError
        def __len__(self):
            return self.l
    Indexable.__mul__ = Amul
    Indexable.__rmul__ = Armul
    overload_for(Indexable)

    f=Indexable()
    np_number = vo[0]
    print('vo[0]*f =',np_number*f)
    # - fails because vo[0] implements its own __mul__
    print('f*vo[0] =',f*np_number)
    print('vo[0] class is',np_number.__class__)
    print('float(vo[0])*f =',np_number.item()*f)
