#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 14:34:25 2018

By analogy with trump_2D

@author: nle003
"""

import numpy as np
from trump import Geometry1D, c_dif, n_dif, c_upc, c_interp_upc

#%% Advection algorithms
def FaceValue_UTOPIA(f,v):
    "vx, vy have dimension of space, i.e. are actual velocity times dt"
    # The flux is fluxx=vxcx*fvx and fluxy=vycy*fvy
    # First-order face value
    assert isinstance(f.geom,Geometry1D) and v.geom is f.geom
    g = f.geom
    fv1 = c_upc(f,v)
    # Third order face value
    # It is only function of df, no f
    df=c_dif(f)
    # Corrected df for 3rd-order method
    nv = g.grid.delta * np.sign(v)
    dfc = c_interp_upc(df,(nv+v)/3.)
    dfv3 = (nv-v)/2.*dfc
    # Face value is
    # f=fv1+dfv3 at (i-1/2)
    return (fv1,dfv3)

# The flux correction algorithm of Leonard [1991]
def arrmin(f1,f2,s): return(f1+f2-s*np.abs(f1-f2))/2.
def arrmin3(f1,f2,f3,s): return arrmin(f1,arrmin(f2,f3,s),s)
def arrmax(f1,f2,s): return (f1+f2+s*np.abs(f1-f2))/2.
TVD_tmp = None
def TVD_ULTIMATE(f,vcr,fv1,dfv3,apply=True):
    "as usual, v has dimensions of length"
    assert isinstance(f.geom,Geometry1D) and vcr.geom is f.geom
    if not apply:
        fluxl=(fv1+dfv3)*vcr
        return fluxl
    da = f.geom.grid.delta
    # All of the following variables have dimensions of f in cartesian or f*r in cylindrical
    # (except tmp,s which are dimensionless)
    #print('a=',a)
    # Leonard's [1991] phiC, phiD and phiU
    #fC = 0 # c_up(f,vcr[a],a) # same as fv1[a]
    dfa = c_dif(f)*da
    fv = dfv3
    fD = np.sign(vcr)*dfa
    CURV = c_upc(n_dif(dfa),vcr)*da # same sign indep of v
    fU = CURV - fD
    DEL= fD - fU
    fref = fU.copy()
    fref.arr = fU.arr.copy()
    inz=(vcr!=0)
    tmp = fU.copy()
    tmp.alloc(np.double)
    tmp.setv = np.abs(vcr)/da
    fref[inz] -= fU[inz]/tmp[inz]
    # Note that when velocity is zero, flux is zero and there is nothing
    # to limit.
    s=np.sign(DEL)
    fvnew = arrmax(arrmin3(fv,fref,fD,s),0,s)
    ic = (np.abs(CURV)>=np.abs(DEL))
    fvnew[ic] = 0
    fluxl = vcr*(fv1 + fvnew)
    return fluxl

# Various algorithms for advection
def AdvectionStepI(f,v,alg=None):
    "vx, vy have dimensions of space, i.e., are actual velocity times dt"
    assert isinstance(f.geom,Geometry1D) and v.geom is f.geom
    g = f.geom
    if (v.arr>g.grid.delta).any():
        print('WARNING: CFL condition is not satisfied in AdvectionStepI')
    cyl_flux_methods = ['CIR','QUICKEST','ULTIMATE']
    if alg in cyl_flux_methods:
        fv1,dfv3=FaceValue_UTOPIA(f,v) # in cylindrical, these are facevalue x radius
    if alg=='QUICKEST' or alg=='ULTIMATE':
        flux = TVD_ULTIMATE(f,v,fv1,dfv3,apply=(alg=='ULTIMATE'))
    if alg=='CIR':
        # First-order upstream method [Courant, Isaacson and Rees, 1952]
        df = -n_dif(v*fv1) # is required df x radius
    elif alg=='MacCormack_Reverse':
        # Stable, 2nd order
        # Even more stable than Lax-Wendroff
        # Slightly better than MacCormack?? About the same
        # 1. Calculate interim value
        fi = f - n_dif(v*c_upc(f,-v))
        df = (-f+fi)/2 - n_dif(v*c_upc(fi,v))/2.
    elif alg=='MacCormack':
        # Stable, 2nd order
        # 1. Calculate interim value
        fi = f - n_dif(v*c_upc(f,v))
        df = (-f+fi)/2 - n_dif(v*c_upc(fi,-v))/2.
    elif alg=='QUICKEST' or alg=='ULTIMATE':
        # Third-order upstream method, without or with flux correction
        df = -n_dif(flux)
    else:
        raise Exception('Unknown advection algorithm: '+alg)
    return df
