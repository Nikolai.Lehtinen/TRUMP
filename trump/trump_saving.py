#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 17:57:17 2018

Extended Array serialization

@author: nle003
"""

import numpy as np
from trump import Grid, Geometry1D, Geometry2D, ExtendedArray1D, ExtendedArray2D, end, span
from trump_2D import Geometry2D_car, Geometry2D_cyl

print('Loading TRUMP serialization routines')

def geom_to_arr(geom):
    if isinstance(geom,Geometry1D):
        g = geom.grid
        return np.array([g.n_cells,g.delta,g.start],dtype=np.double)
    elif isinstance(geom,Geometry2D):
        g1,g2 = geom.grids
        basic = np.array([g1.n_cells,g1.delta,g1.start,g2.n_cells,g2.delta,g2.start],dtype=np.double)
        if isinstance(geom,Geometry2D_car) or isinstance(geom,Geometry2D_cyl):
            flag = 1 if isinstance(geom,Geometry2D_car) else 2
            extra = np.array([flag,geom.xs[0].nl,geom.xs[1].nl,geom.xs[0].nu,geom.xs[1].nu],dtype=np.double)
            return np.hstack((basic,extra))
        else:
            return basic

def arr_to_geom(arr):
    n = len(arr)
    assert n==3 or n==6 or n==11
    gridx = Grid(np.int(arr[0]),arr[1],arr[2])
    if n==3:
        # 1D
        return Geometry1D(gridx)
    else:
        # Basic 2D
        gridy = Grid(np.int(arr[3]),arr[4],arr[5])
        if n==6:
            return Geometry2D(gridx,gridy)
        else:
            flag = np.int(arr[6])
            assert flag==1 or flag==2
            nls = (np.int(arr[7]),np.int(arr[8]))
            nus = (np.int(arr[9]),np.int(arr[10]))
            if flag==1:
                return Geometry2D_car(gridx,gridy,nls,nus)
            else:
                return Geometry2D_cyl(gridx,gridy,nls,nus)

def same_geom(g1,g2):
    if not isinstance(g1,np.ndarray):
        g1 = geom_to_arr(g1)
    if not isinstance(g2,np.ndarray):
        g2 = geom_to_arr(g2)
    return np.array_equal(g1,g2)

def ea_to_tuple(ea):
    if isinstance(ea,ExtendedArray1D):
        info = np.array([ea.stag,ea.nl,ea.nu],dtype=np.int)
    elif isinstance(ea,ExtendedArray2D):
        info = np.array([ea.stags[0],ea.stags[1],ea.nls[0],ea.nls[1],ea.nus[0],ea.nus[1]],dtype=np.int)
    return (geom_to_arr(ea.geom),info,ea.arr)

def tuple_to_ea(geom,info,arr):
    if isinstance(geom,np.ndarray):
        geom = arr_to_geom(geom)
    if len(info)==3:
        f = ExtendedArray1D(geom,stag=info[0],nl=info[1],nu=info[2])
    elif len(info)==6:
        f = ExtendedArray2D(geom,stags=(info[0],info[1]),nls=(info[2],info[3]),nus=(info[4],info[5]))
    f.arr = arr
    return f

#%%
#def dict_to_ea(name,d):
#    info = d[name+'_EAI']
#    if len(info)==6:
#        # ExtendedArray1D
#        grid = Grid(np.int(info[0]),info[1],info[2])
#        f = ExtendedArray1D(Geometry1D(grid),stag=np.int(info[3]),nl=np.int(info[4]),nu=np.int(info[5]))
#    elif len(info)==12:
#        # ExtendedArray2D
#        grid1 = Grid(np.int(info[0]),info[1],info[2])
#        grid2 = Grid(np.int(info[3]),info[4],info[5])
#        stags = (np.int(info[6]),np.int(info[7]))
#        nls = (np.int(info[8]),np.int(info[9]))
#        nus = (np.int(info[10]),np.int(info[11]))
#        f = ExtendedArray2D(Geometry2D(grid1,grid2),stags=stags,nls=nls,nus=nus)
#    f.arr = d[name+'_EAA']
#    return f

#%%
if __name__=='__main__':
    from trump_2D import ea_plot
    #%%
    gridx = Grid(10,.1,0)
    gridy = Grid(5,.1,0)
    geom = Geometry1D(gridx)
    f = ExtendedArray1D(geom,stag=0,nl=2,nu=2)
    f.arr = np.arange(-2,13,dtype=np.double)
    tf = ea_to_tuple(f)
    assert same_geom(tf[0],geom)
    f_r = tuple_to_ea(geom,tf[1],tf[2])
    plt.figure()
    ea_plot(f)
    ea_plot(f_r,plotargs=('x'))
    #%%
    geom2 = Geometry2D(gridx,gridy)
    p = ExtendedArray2D(geom2,stags=(0,0),nls=(2,2),nus=(2,2))
    p.arr = np.sin(p.xe(0)[:,np.newaxis]*2*np.pi) + np.cos(p.xe(1)[np.newaxis,:]*2*np.pi)
    tp = ea_to_tuple(p)
    assert same_geom(tp[0],geom2)
    p_r = tuple_to_ea(geom2,tp[1],tp[2])
    plt.figure()
    ea_plot(p-p_r)
