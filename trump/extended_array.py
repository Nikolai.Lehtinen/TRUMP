#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 13:33:08 2016

Extended array with a "normal" indexing system, i.e., negative indices give the
element in the extention on the lower side; and the "stop" element of a slice is
the last index (like in MATLAB).

Interface:
    
Stage 1 (obligatory)
    f = ExtendedArray1D(10,stag=0,nl=1,nu=1)
    f.n == 10
    f[0] is the first element
    f[f.n-1] is the last element
    f[f.n] is the first extention element on the upper side
    f[-1] is the first extension element on the lower side
    f[-2] and f[f.n+1] raise an IndexError for this value of f
    # Slice support:
    f[:] or f.val are elements from 0 to f.n-1
    f[-2:f.n+1] raises an IndexError for this value of f
    f[:2] or f[0:2] includes f[0], f[1], and f[2]
    
Stage 2: The "end" keyword
    f[end] is the last element, same as f[f.n-1]
    f[end+1] is the first extention element on the upper side
    f[end+2] raise an IndexError for this value of f
    f[-2:end+2] raises an IndexError for this value of f
    f[end-1:end+1] includes f[end-1], f[end] and f[end+1]
    f[end-1:end] is the same as f[end-1:]
    f[:] is equivalent to f[0:end]
    
Stage 3: iteration (not implemented)
    for elem in f.arr:
        # Iterates over all elements
    for elem in f.val:
        # Iterates only over f.val
        
Stage 4: Boolean indexing (not implemented)
    The index array must be also an ExtendedArray1D
    
Stage 5: Fancy indexing
    
@author: nle003
"""

#%% Imports
import numpy as np
import numbers
from collections import namedtuple
#import trump
import trump.symbolic_array as sy

def isnumber(n):
    return isinstance(n,numbers.Number)
def replace_in_tuple(t,a,new):
    return t[:a]+(new,)+t[a+1:]
    
#%% Grid description
# n_cells is number of cells, not nodes!
Grid=namedtuple('Grid',['n_cells','delta','start'])
def grid_stop(grid):
    "The last point of a geometry"
    return grid.start + grid.n_cells*grid.delta
def grid_n(grid,stag):
    "Number of main points in a geometry"
    assert (stag==0 or stag==1)
    return  grid.n_cells + 1 - stag
def grid_x(grid,stag,nl=0,nu=0):
    "Coordinates of the given configuration"
    assert (stag==0 or stag==1)
    d = grid.delta
    start = grid.start + (stag/2)*d
    n = grid.n_cells + 1 - stag
    return start + np.arange(-nl,n+nu)*d
    # Was:
    #return np.linspace(start-d*nl, start+(n+nu-1)*d, n+nl+nu)
def grid_ea_x(geom,stag,nl=0,nu=0):
    "The ExtendedArray1D version of the previous"
    assert (stag==0 or stag==1)
    if isinstance(geom,Grid): # we were given a grid instead of geometry
        geom=Geometry1D(geom)
    xtmp = ExtendedArray1D(geom,stag,nl,nu)
    xtmp.arr = grid_x(geom.grid,stag,nl=nl,nu=nu)
    return xtmp

#%% 1D geometry - a more abstract than grid
# We use it because it can store extra user stuff and the extended arrays will share it
class Geometry: pass # abstract
class Geometry1D(Geometry):
    def __init__(self,grid):
        self.grid = grid
        # Coordinates used for interpolation of functions
    def calculate_x(self,nl=0,nu=0):
        self.x = grid_ea_x(self,stag=0,nl=nl,nu=nu)
        self.xc = grid_ea_x(self,stag=1,nl=nl+1,nu=nu+1)

#%% End index
class EndIndex:
    def __init__(self,i=0):
        self.i = i
    def __add__(self,i):
        "Returns an index"
        assert isinstance(i,numbers.Integral)
        return EndIndex(self.i+i)
        pass
    def __sub__(self,i):
        return self + (-i)
    def __call__(self,ea):
        assert isinstance(ea,ExtendedArray)
        if isinstance(ea,ExtendedArray1D):
            return self.i - 1 + ea.n
        elif isinstance(ea,ExtendedArray2D):
            return tuple(self.i - 1 + n for n in ea.ns)
    #def __repr__(self):
    #    "The most generic __repr__"
    #    return '%s(%s)' % (self.__class__.__name__, \
    #        ', '.join('%s=%s' % (k,repr(self.__dict__[k])) \
    #                                   for k in sorted(self.__dict__)))
    def __repr__(self):
        if self.i==0:
            return 'end'
        else:
            return 'end{:+d}'.format(self.i)
    pass

end = EndIndex() # Should be a constant! But in Python, no way to enforce it

def to_i(ei,n):
    if isinstance(ei,EndIndex):
        return ei.i + n - 1
    else:
        return ei
    pass

class SpanSlice:
    "The whole range of value along an axis"
    def __call__(self,ea):
        if isinstance(ea,ExtendedArray1D):
            return slice(-ea.nl,end+ea.nu)
        elif isinstance(ea,ExtendedArray2D):
            return (slice(-ea.nls[0],end+ea.nus[0]),slice(-ea.nls[1],end+ea.nus[1]))
span = SpanSlice() # another global, like "end"

#%%
def info_index(n,nl,nu,s):
    return '{:d}|{:d}|{:d}<{:s}>'.format(nl,n,nu,'n' if s==0 else 'c')

def reslice(i,nl,nu,n,nt,a=None):
    """
    Convert ExtendedArray slice to a Python array slice.
    Who would have thought that arr[0:1:1] is arr[0] but arr[0:-1:-1] is empty?
    And other idiotisms of Python index system. Good we don't have to convert the other
    way around.
    Added on 2017/11/28: allow returning empty array for indices like [11:10] or [13:11:2]
    but not [12:10] or [14:11:2] (the negative difference is too high for step).
    """
    if i is span:
        return slice(None)
    text = ' out of bounds [{}:{}]'.format(-nl,n+nu-1)
    if not a is None:
        text += ' on axis {}'.format(a)
    if isinstance(i,slice):
        step = (1 if i.step is None else i.step)
        sign = (1 if step>0 else -1)
        if step == 0: raise ValueError('slice step is zero')
        # START
        if i.start is None:
            start = (nl if step>0 else nl+n-1)
        else:
            start = nl + to_i(i.start,n)
        if start < 0 or start >= nt:
            raise IndexError('start {}'.format(start-nl)+text)
        # STOP
        if i.stop is None:
            stop = (nl+n if step>0 else nl-1)
        else:
            stop = nl + to_i(i.stop,n) + sign
        if stop > nt or stop < -1:
            raise IndexError('stop {}'.format(stop-nl-sign)+text)
        if (step>0 and start>=stop+step) or (step<0 and start<=stop+step):
            gt_or_lt = (' > ' if step>0 else ' < ')
            delta = '{:+d}'.format(-step)
            raise ValueError(('(start=={})'+delta+gt_or_lt+'(stop=={}), step=={}').format(
                             start-nl,stop-nl-sign,step))
        if stop == -1: # This is tricky, we cannot have -1
            stop = None
        i = slice(start,stop,i.step)
    elif isinstance(i,numbers.Integral) or isinstance(i,EndIndex):
        i = to_i(i,n)
        if (i<-nl or i>=n+nu):
            raise IndexError('index {}'.format(i)+text)
        i += nl
        #print('i=',i)
    elif isinstance(i,np.ndarray):
        assert i.dtype is np.dtype('int')
        i = i.copy() + nl # do not mess up the original
    else:
        raise IndexError('unimplemented index type')
    return i

def ea_view_slice(s,nl,nu,n,nt,a=None):
    if s is span:
        return (nl,nu,slice(None))
    if not isinstance(s,slice):
        raise IndexError('EA view can be only slice')
    if not (s.step is None or s.step==1):
        raise IndexError('EA view can only be forward')
    if not (s.start is None or isinstance(s.start,numbers.Integral)):
        raise IndexError('EA view start index is invalid')
    if not (s.stop is None or isinstance(s.stop,EndIndex)):
        raise IndexError('EA view stop index is invalid')
    if s.start is None:
        nl_new = 0
    else:
        nl_new = -s.start
    if s.stop is None:
        nu_new = 0
    else:
        nu_new = s.stop.i
    start = nl - nl_new
    text = ' out of bounds [{}:{}]'.format(-nl,n+nu-1)
    if not a is None:
        text += ' on axis {}'.format(a)
    if start < 0 or start >= nt:
        raise IndexError('start {}'.format(start - nl)+text)
    stop = nl + n + nu_new
    if stop > nt or stop < -1:
        raise IndexError('stop {}'.format(stop - nl - 1)+text)
    return (nl_new,nu_new,slice(start,stop,None))

class ExtendedArray: pass # Abstract class

###############################################################################
#%% 1D PART
###############################################################################
#%% Extended Array narrowed view
class ExtendedArray1DView:
    """An object that allows limited subindexing of an ExtendedArray1D
    without copying. Indexing returns an ExtendedArray1D object.
    Example:
        geom=Geom1D(n_cells=10,delta=0.1,start=0)
        rho=Field1D('rho',geom=geom,stag=0,nl=1,nu=1)
        phi=Field1D('phi',geom=geom,stag=0,nl=0,nu=0)
        phi.bc[0]=0; phi.bc[end]=0; phi.bc.freeze()
        rho[:]=1
        Solver1D(phi,-n_dif(c_dif(phi.symb))).solve_full(phi,rho.view[1:end-1])
        plt.plot(geom_x(geom,0),phi.val)
    """
    def __init__(self,ea):
        self.ea = ea
    def __getitem__(self,s):
        "Like getitem (with limited capability), and return an ExtendedArray1D"
        (nl_new,nu_new,s_new) = ea_view_slice(s,
            self.ea.nl,self.ea.nu,self.ea.n,self.ea.nt)
        res = ExtendedArray1D(self.ea.geom,self.ea.stag,nl_new,nu_new)
        res.arr = self.ea.arr[s_new]
        return res

#%% Extended Array
class ExtendedArray1D(ExtendedArray):
    def __init__(self,geom,stag,nl,nu,trace=False):
        assert not geom is None and not stag is None and \
            not nl is None and not nu is None
        self.geom = geom # it is important that this is a handle, not a copy! can use "is"
        assert stag is not None
        self.stag = stag
        n = grid_n(geom.grid,stag)
        self.nt = n + nl + nu
        self.n = n
        self.nl = nl
        self.nu = nu
        # Can get a sub-extended-array. Note: it is a view, they share data!
        self.view = ExtendedArray1DView(self)
        self.trace = trace
        self.arr = None
        self.is_integer = False
    @property
    def arr(self):
        return self._arr
    @arr.setter
    def arr(self,v):
        assert v is None or len(v)==self.nt
        self._arr = v
        if v is not None and (not self.is_integer) and (not isinstance(v,sy.CAExpression)) \
                and np.issubdtype(v.dtype, np.integer):
            raise Exception('Integer arrays lead to trouble, set ea.is_integer=True to override')
        if self.trace:
            if v is None:
                self.touched = None
            else:
                self.touched = np.ones((self.nt,),dtype=np.bool)
    def copy(self,trace=None):
        return ExtendedArray1D(self.geom,self.stag,self.nl,self.nu,
                               self.trace if trace is None else trace)
    @property
    def is_symb(self):
        assert self.arr is not None
        return isinstance(self.arr,sy.CAExpression)
    def alloc(self,dtype):
        self.arr = np.zeros((self.nt,),dtype=dtype)
        if self.trace:
            self.touched = np.zeros((self.nt,),dtype=np.bool)
    def alloc_symbolic(self,name):
        self._arr = sy.SymbolicArray(name,self.nt,array_like=True)
    def _index(self,i):
        """Can take positive and negative integers and EndClass
        Or a boolean ExtendedArray1D"""
        if isinstance(i,ExtendedArray1D) and i.arr.dtype==np.bool:
            # A hack: logical index
            iea=ExtendedArray1D.copy(self)
            iea.arr=np.ones((self.nt,),dtype=np.bool)
            iea &= i
            return iea.arr
        else:
            return reslice(i,self.nl,self.nu,self.n,self.nt)
    def restore_index(self,i):
        "Reverse operation to _index, only for scalars"
        return i-self.nl
    def __getitem__(self,i):
        if self.arr is None:
            raise TypeError('Allocate and set values first')
        return self.arr[self._index(i)]
    def __setitem__(self,i,v):
        if self.arr is None:
            raise TypeError('Allocate first')
        self.arr[self._index(i)]=v
        if self.trace:
            self.touched[self._index(i)]=True
    def get_value(self):
        "Do not call"
        raise Exception('Do not call ExtendedArray1D.setv as getter, use ea.arr or ea[:]')
        #return self[:]
    def set_value(self,rhs):
        "Assign maximum possible number of values"
        if self.arr is None:
            raise TypeError('Allocate first')
        assert isinstance(rhs,ExtendedArray1D)
        assert (self.geom is rhs.geom and self.stag == rhs.stag and not rhs.arr is None)
        nl=min(self.nl,rhs.nl)
        nu=min(self.nu,rhs.nu)
        self.arr[self.nl-nl:self.nt-self.nu+nu] = rhs.arr[rhs.nl-nl:rhs.nt-rhs.nu+nu]
        if self.trace:
            self.touched[self.nl-nl:self.nt-self.nu+nu] = True
    setv = property(get_value,set_value)
    @property
    def val(self):
        "Compatibility issues"
        raise Exception('OBSOLETE, use ea.setv')
    def _neighbor(self):
        return ExtendedArray1D(self.geom,1-self.stag,
            nl=self.nl-self.stag,nu=self.nu-self.stag)
    def lo(self):
        "lo() and hi() are the the values shifted by half cell"
        res=self._neighbor()
        res.arr = self.arr[:-1]
        return res
    def hi(self):
        "See lo()"
        res = self._neighbor()
        res.arr = self.arr[1:]
        return res
    @property
    def x(self):
        "x() are coordinates where the useful part of the array is defined"
        return grid_x(self.geom.grid,self.stag)
    @property
    def xe(self):
        "Coordinates where the whole array (with extra points) is defined"
        return grid_x(self.geom.grid,self.stag,nl=self.nl,nu=self.nu)
    @property
    def pcolor_x(self):
        "Same as self.ea_h().arr[self.nl:-self.nu]"
        s = 1 - self.stag
        return grid_x(self.geom.grid,s,nl=s,nu=s)
    @property
    def pcolor_xe(self):
        s = 1 - self.stag
        return grid_x(self.geom.grid,s,nl=self.nl+s,nu=self.nu+s)
    def ea_x(self):
        return grid_ea_x(self.geom,self.stag,nl=self.nl,nu=self.nu)
    def __repr__(self):
        return self.info()
    def info(self,name='ExtendedArray1D'):
        return '{:s}[{:s}]'.format(name,info_index(self.n,self.nl,self.nu,self.stag))
    pass

#%% Adding more stuff outside class definition
BINARY_OPERATORS = ['add','radd','sub','rsub','mul','rmul','pow','rpow',
    'truediv','rtruediv','floordiv','rfloordiv','mod','rmod',
    'and','rand','or','ror','xor','eq','ne','gt','lt','ge','le']
UNARY_OPERATORS = ['neg','pos','abs','invert']
INPLACE_OPERATORS = ['iadd','isub','imul','itruediv','ipow','iand','ior']
NUMPY_FUNCTIONS = ['sign','sqrt','exp','sin','cos','tan','sinh','cosh','tanh','log','log10',
                   'arcsin','arccos','arctan','arctan2','arcsinh','arccosh','arctanh']
# Monkey-patching galore of the NumPy operators and functions
def make_binary_fun_ea1(op):
    def closed_binary(t1,t2):
        assert not t1.arr is None
        if isinstance(t2,ExtendedArray1D):
            if not (t1.geom is t2.geom and t1.stag == t2.stag):
                raise TypeError('in operation '+op+' operands must match')
            # Must align the "visible" part and keep as many invisible as
            # possible
            nl=min(t1.nl,t2.nl)
            nu=min(t1.nu,t2.nu)
            res = ExtendedArray1D(t1.geom, t1.stag, nl=nl, nu=nu)
            # Assign max possible values
            res.arr = getattr(t1.arr[t1.nl-nl:t1.nt-t1.nu+nu],op)\
                            (t2.arr[t2.nl-nl:t2.nt-t2.nu+nu])
        elif t2 is None:
            raise TypeError('cannot apply '+op+' to ExtendedArray1D and None')
        else:
            res = ExtendedArray1D.copy(t1) # do not allow derived class copy
            res.arr = getattr(t1.arr,op)(t2)
        return res
    return closed_binary
for bop in BINARY_OPERATORS:
    bopd='__'+bop+'__'
    setattr(ExtendedArray1D,bopd,make_binary_fun_ea1(bopd))
def make_unary_fun_ea1(op):
    def closed_unary(t):
        assert not t.arr is None
        res=ExtendedArray1D.copy(t)
        res.arr=getattr(t.arr,op)()
        return res
    return closed_unary
for uop in UNARY_OPERATORS:
    uopd = '__'+uop+'__'
    setattr(ExtendedArray1D,uopd,make_unary_fun_ea1(uopd))
def make_inplace_fun_ea1(op):
    def closed_inplace(t1,t2):
        assert not t1.arr is None
        if isinstance(t2,ExtendedArray1D):
            # Update the biggest possible portion
            if not (t1.geom is t2.geom and t1.stag == t2.stag):
                raise TypeError('in operation '+op+' operands must match')
            nl=min(t1.nl,t2.nl)
            nu=min(t1.nu,t2.nu)
            # Assign max possible values
            getattr(t1.arr[t1.nl-nl:t1.nt-t1.nu+nu],op)\
                            (t2.arr[t2.nl-nl:t2.nt-t2.nu+nu])
            if t1.trace:
                t1.touched[t1.nl-nl:t1.nt-t1.nu+nu]=True
        else:
            getattr(t1.arr,op)(t2)
            if t1.trace:
                t1.touched[:]=True
        return t1
    return closed_inplace
for iop in INPLACE_OPERATORS:
    iopd = '__'+iop+'__'
    setattr(ExtendedArray1D,iopd,make_inplace_fun_ea1(iopd))

#%% Decorate NumPy functions
class NumpyOverrideContext1D:
    numinstances=0
    def __init__(self,fun_list):
        if self.__class__.numinstances > 0:
            raise Exception('Cannot have more then one '+self.__class__.__name__)
        self.__class__.numinstances += 1
        self.entered=False
        self.fun_list = fun_list
        self.saved_np_functions={}
    def make_discrete(self,np_fun):
        if np_fun.__name__ in self.saved_np_functions:
            return np_fun
        #print('ExtendedArray1D overriding ',np_fun.__name__)
        self.saved_np_functions[np_fun.__name__] = np_fun
        def new_fun(a,*args,**kws):
            if isinstance(a,ExtendedArray1D):
                b=ExtendedArray1D.copy(a)
                b.arr=np_fun(a.arr,*args,**kws)
            else:
                b=np_fun(a,*args,**kws)
            return b
        new_fun.__name__=np_fun.__name__
        return new_fun
    def enter(self):
        if self.entered:
            print('You cannot enter the same river twice')
            return
        # The main stuff occurs here, i.e., entering the context
        print('Extending 1D NumPy functions')
        for fun in self.fun_list:
            #print('Decorating ',fun)
            setattr(np,fun, self.make_discrete(getattr(np,fun)))
        self.entered=True
    def exit(self):
        if not self.entered:
            print('To exit, you need to enter first')
            return
        for fun in self.saved_np_functions:
            setattr(np,fun, self.saved_np_functions[fun])
        self.saved_np_functions={}
        self.entered=False
        print('NumPy 1D functions restored')
    def __del__(self):
        if self.entered: self.exit()
        print('Deleting',self.__class__.__name__)

OVERRIDE_NUMPY_FUNCTIONS_CONTEXT_1D = \
    NumpyOverrideContext1D(NUMPY_FUNCTIONS)

###############################################################################
#%% 2D PART
###############################################################################

#%% 2D geometry
class Geometry2D(Geometry):
    def __init__(self,grid1,grid2):
        self.grids = (grid1,grid2)
        # Coordinates used for interpolation of functions
    def Grad(self,f):
        assert f.geom is self
        return (c_dif(f,0),c_dif(f,1))
    def Laplacian(self,f):
        assert f.geom is self
        return self.Div(*self.Grad(f))
    def calculate_xs(self,nls=(0,0),nus=(0,0)):
        self.xs = tuple(grid_ea_x(self.grids[k],stag=0,nl=nls[k],nu=nus[k]) for k in range(2))
        self.xcs = tuple(grid_ea_x(self.grids[k],stag=1,nl=nls[k]+1,nu=nus[k]+1) for k in range(2))

#%%
def fancy_index(ii,shape):
    if True:
        i0=np.arange(np.prod(shape)).reshape(shape)
        return i0[ii].flatten()
    else:
        i0=np.zeros(shape,dtype=np.bool)
        i0[ii]=True
        return i0.flatten()

#%% Extended array narrowed view
class ExtendedArray2DView:
    def __init__(self,ea):
        self.ea = ea
    def __getitem__(self,s2):
        assert isinstance(s2,tuple) and len(s2)==2
        tmp = ()
        for (a,s) in enumerate(s2):
             tmp += (ea_view_slice(
                s,self.ea.nls[a],self.ea.nus[a],self.ea.ns[a],self.ea.nts[a],a),)
        #print(tmp)
        nls_new,nus_new,ss_new = zip(*tmp)
        res = ExtendedArray2D(self.ea.geom,self.ea.stags,nls_new,nus_new)
        res.arr = self.ea.arr[self.ea._fancy(ss_new)]
        return res

#%% Extended Array, hard-coded 2D, see extended_array_nd for general n
class ExtendedArray2D(ExtendedArray):
    "Want to store data in a 1D array, to make compatible with SymbolicArray etc"
    def __init__(self,geom,stags,nls,nus,trace=False):
        assert not geom is None and not stags is None and not nls is None and not nus is None
        self.geom = geom
        self.stags = stags
        ns = (grid_n(geom.grids[0],stags[0]), grid_n(geom.grids[1],stags[1]))
        self.nts = (ns[0] + nls[0] + nus[0], ns[1]+nls[1]+nus[1])
        self.ns = ns
        self.nls = nls
        self.nus = nus
        self.view = ExtendedArray2DView(self)
        self.trace = False # not used yet
        self.arr = None
    def copy(self):
        return ExtendedArray2D(self.geom,self.stags,self.nls,self.nus)
    @property
    def arr(self):
        return self._arr
    @arr.setter
    def arr(self,v):
        assert v is None or (isinstance(v,sy.CAExpression) and len(v)==np.prod(self.nts))\
            or v.shape==self.nts
        self._arr = v
    @property
    def is_symb(self):
        assert not self.arr is None
        return isinstance(self.arr,sy.CAExpression)
    def alloc(self,dtype):
        self.arr = np.zeros(self.nts,dtype=dtype)
    def alloc_symbolic(self,name):
        self.arr = sy.SymbolicArray(name,np.prod(self.nts),array_like=True)
    def _index(self,i2):
        "Can take positive and negative integers and EndClass and logical index"
        if isinstance(i2,tuple) and len(i2)==2:
            ii = ()
            for (a,i) in enumerate(i2):
                ii += (reslice(i,self.nls[a],self.nus[a],self.ns[a],self.nts[a],a),)
            # Now, ii is a regular Python index
            return self._fancy(ii)
        elif isinstance(i2,ExtendedArray2D) and i2.arr.dtype==np.bool:
            # A hack: logical index
            iea=ExtendedArray2D.copy(self)
            iea.arr=np.ones(self.nts,dtype=np.bool)
            iea &= i2
            return iea.arr
        else:
            raise IndexError('Unsupported index type')
    def restore_index(self,i):
        "Reverse operation to _index, only for scalars"
        tmp = np.unravel_index(i,self.nts)
        return (tmp[0]-self.nls[0],tmp[1]-self.nls[1])
    def _fancy(self,ii):
        if self.is_symb:
            return fancy_index(ii,self.nts)
        else:
            # A regular ndarray
            return ii
    def __getitem__(self,i):
        if self.arr is None:
            raise TypeError('Allocate and set values first')
        return self.arr[self._index(i)]
    def __setitem__(self,i,v):
        if self.arr is None:
            raise TypeError('Allocate first')
        self.arr[self._index(i)]=v
    def get_value(self):
        "Do not call"
        raise Exception('Do not call ExtendedArray2D.setv as getter, use ea.arr or ea[:,:]')
        #return self[:,:]
    def set_value(self,rhs):
        "Assign maximum possible number of values"
        if self.arr is None:
            raise TypeError('Allocate first')
        assert isinstance(rhs,ExtendedArray2D)
        assert (self.geom is rhs.geom and self.stags==rhs.stags and not rhs.arr is None)
        nls=(min(self.nls[0],rhs.nls[0]), min(self.nls[1],rhs.nls[1]))
        nus=(min(self.nus[0],rhs.nus[0]), min(self.nus[1],rhs.nus[1]))
        # Assign max possible values
        i1 = (slice(self.nls[0]-nls[0],self.nts[0]-self.nus[0]+nus[0]),
              slice(self.nls[1]-nls[1],self.nts[1]-self.nus[1]+nus[1]))
        i2 = (slice(rhs.nls[0]-nls[0],rhs.nts[0]-rhs.nus[0]+nus[0]),
              slice(rhs.nls[1]-nls[1],rhs.nts[1]-rhs.nus[1]+nus[1]))          
        if self.is_symb or rhs.is_symb:
            i1 = fancy_index(i1,self.nts)
            i2 = fancy_index(i2,rhs.nts)
            self.arr.flat[i1]=rhs.arr.flat[i2]
        else:
            self.arr[i1]=rhs.arr[i2]
        if self.trace:
            self.touched[i1] = True
    setv = property(get_value,set_value)
    @property
    def val(self):
        "Compatibility issues"
        raise Exception('OBSOLETE, use ea.setv')
    def _neighbor(self,a):
        assert not self.arr is None
        s = self.stags
        if a==0:
            return ExtendedArray2D(self.geom, stags = (1-s[0],s[1]),
                nls = (self.nls[0]-s[0],self.nls[1]),
                nus = (self.nus[0]-s[0],self.nus[1]))
        elif a==1:
            return ExtendedArray2D(self.geom, stags = (s[0],1-s[1]),
                nls = (self.nls[0],self.nls[1]-s[1]),
                nus = (self.nus[0],self.nus[1]-s[1]))
        else: raise Exception('a is invalid')
    def lo(self,a):
        "lo() and hi() are the the values shifted by half cell"
        res=self._neighbor(a)
        if a==0:
            res.arr = self.arr[self._fancy((slice(None,-1),slice(None)))]
        else:
            res.arr = self.arr[self._fancy((slice(None),slice(None,-1)))]
        return res
    def hi(self,a):
        "See lo()"
        res=self._neighbor(a)
        if a==0:
            res.arr = self.arr[self._fancy((slice(1,None),slice(None)))]
        else:
            res.arr = self.arr[self._fancy((slice(None),slice(1,None)))]
        return res
    def x(self,a):
        "x() are coordinates where the useful part of the array is defined"
        return grid_x(self.geom.grids[a],self.stags[a])
    def xe(self,a):
        "Coordinates where the whole array (with extra points) is defined"
        return grid_x(self.geom.grids[a],self.stags[a],nl=self.nls[a],nu=self.nus[a])
    def ea_x(self,a):
        return grid_ea_x(self.geom.grids[a],self.stags[a],nl=self.nls[a],nu=self.nus[a])
    def pcolor_x(self,a):
        "Such that ave(pcolor_x)==x"
        s = 1-self.stags[a]
        return grid_x(self.geom.grids[a],s,s,s)
    def pcolor_xe(self,a):
        "Such that ave(pcolor_xe)==xe"
        s = 1 - self.stags[a]
        return grid_x(self.geom.grids[a],s,self.nls[a]+s,self.nus[a]+s)
    def __repr__(self):
        return self.info()
    def info(self,name='ExtendedArray2D'):
        return '{:s}[{:s}, {:s}]'.format(name,
                info_index(self.ns[0],self.nls[0],self.nus[0],self.stags[0]),
                info_index(self.ns[1],self.nls[1],self.nus[1],self.stags[1]))
    pass

#%% Adding more stuff outside class definition
# Monkey-patching galore of the NumPy operators and functions
def make_binary_fun_ea2(op):
    def closed_binary(t1,t2):
        assert not t1.arr is None
        if isinstance(t2,ExtendedArray2D):
            if not (t1.geom is t2.geom and t1.stags == t2.stags):
                raise TypeError('in operation '+op+' operands must match')
            # Must align the "visible" part and keep as many invisible as
            # possible
            nls=(min(t1.nls[0],t2.nls[0]), min(t1.nls[1],t2.nls[1]))
            nus=(min(t1.nus[0],t2.nus[0]), min(t1.nus[1],t2.nus[1]))
            res = ExtendedArray2D(t1.geom, t1.stags, nls=nls, nus=nus)
            # Assign max possible values
            i1 = (slice(t1.nls[0]-nls[0],t1.nts[0]-t1.nus[0]+nus[0]),
                  slice(t1.nls[1]-nls[1],t1.nts[1]-t1.nus[1]+nus[1]))
            i2 = (slice(t2.nls[0]-nls[0],t2.nts[0]-t2.nus[0]+nus[0]),
                  slice(t2.nls[1]-nls[1],t2.nts[1]-t2.nus[1]+nus[1]))
            if t1.is_symb or t2.is_symb:
                # The result will also by symbolic
                i1 = fancy_index(i1,t1.nts)
                i2 = fancy_index(i2,t2.nts)
                res.arr = getattr(t1.arr.flat[i1],op)(t2.arr.flat[i2])
            else:
                res.arr = getattr(t1.arr[i1],op)(t2.arr[i2])
        elif t2 is None:
            raise TypeError('cannot apply '+op+' to ExtendedArray2D and None')
        else:
            res = ExtendedArray2D.copy(t1) # do not allow derived class copy
            res.arr = getattr(t1.arr,op)(t2)
        return res
    return closed_binary
for bop in BINARY_OPERATORS:
    bopd='__'+bop+'__'
    setattr(ExtendedArray2D,bopd,make_binary_fun_ea2(bopd))
def make_unary_fun_ea2(op):
    def closed_unary(t):
        assert not t.arr is None
        res=ExtendedArray2D.copy(t)
        res.arr=getattr(t.arr,op)()
        return res
    return closed_unary
for uop in UNARY_OPERATORS:
    uopd = '__'+uop+'__'
    setattr(ExtendedArray2D,uopd,make_unary_fun_ea2(uopd))
def make_inplace_fun_ea2(op):
    def closed_inplace(t1,t2):
        assert not t1.arr is None
        if isinstance(t2,ExtendedArray2D):
            # Update the biggest possible portion
            if not (t1.geom is t2.geom and t1.stags == t2.stags):
                raise TypeError('in operation '+op+' operands must match')
            nls=(min(t1.nls[0],t2.nls[0]), min(t1.nls[1],t2.nls[1]))
            nus=(min(t1.nus[0],t2.nus[0]), min(t1.nus[1],t2.nus[1]))
            # Assign max possible values
            i1 = (slice(t1.nls[0]-nls[0],t1.nts[0]-t1.nus[0]+nus[0]),
                  slice(t1.nls[1]-nls[1],t1.nts[1]-t1.nus[1]+nus[1]))
            i2 = (slice(t2.nls[0]-nls[0],t2.nts[0]-t2.nus[0]+nus[0]),
                  slice(t2.nls[1]-nls[1],t2.nts[1]-t2.nus[1]+nus[1]))          
            if t1.is_symb or t2.is_symb:
                i1 = fancy_index(i1,t1.nts)
                i2 = fancy_index(i2,t2.nts)
                getattr(t1.arr.flat[i1],op)(t2.arr.flat[i2])
            else:
                getattr(t1.arr[i1],op)(t2.arr[i2])
        else:
            getattr(t1.arr,op)(t2)
        return t1
    return closed_inplace
for iop in INPLACE_OPERATORS:
    iopd = '__'+iop+'__'
    setattr(ExtendedArray2D,iopd,make_inplace_fun_ea2(iopd))

#%% Decorate NumPy functions
class NumpyOverrideContext2D:
    numinstances=0
    def __init__(self,fun_list):
        if self.__class__.numinstances > 0:
            raise Exception('Cannot have more then one '+self.__class__.__name__)
        self.__class__.numinstances += 1
        self.entered=False
        self.fun_list = fun_list
        self.saved_np_functions={}
    def make_discrete(self,np_fun):
        if np_fun.__name__ in self.saved_np_functions:
            return np_fun
        #print('ExtendedArray2D overriding ',np_fun.__name__)
        self.saved_np_functions[np_fun.__name__] = np_fun
        def new_fun(a,*args,**kws):
            if isinstance(a,ExtendedArray2D):
                b=ExtendedArray2D.copy(a)
                b.arr=np_fun(a.arr,*args,**kws)
            else:
                b=np_fun(a,*args,**kws)
            return b
        new_fun.__name__=np_fun.__name__
        return new_fun
    def enter(self):
        if self.entered:
            print('You cannot enter the same river twice')
            return
        # The main stuff occurs here, i.e., entering the context
        print('Extending 2D NumPy functions')
        for fun in self.fun_list:
            #print('Decorating ',fun)
            setattr(np,fun, self.make_discrete(getattr(np,fun)))
        self.entered=True
    def exit(self):
        if not self.entered:
            print('To exit, you need to enter first')
            return
        for fun in self.saved_np_functions:
            setattr(np,fun, self.saved_np_functions[fun])
        self.saved_np_functions={}
        self.entered=False
        print('NumPy 2D functions restored')
    def __del__(self):
        if self.entered: self.exit()
        print('Deleting',self.__class__.__name__)

OVERRIDE_NUMPY_FUNCTIONS_CONTEXT_2D = \
    NumpyOverrideContext2D(NUMPY_FUNCTIONS)

#%% Elementary operations which are not user-accessible, no error-checking!
def _dif(f,a=None):
    if a is None: # ExtendedArray1D
        return (f.hi()-f.lo())/f.geom.grid.delta
    else: # ExtendedArray2D
        return (f.hi(a)-f.lo(a))/f.geom.grids[a].delta
def _ave(f,a=None):
    if a is None: return (f.hi() + f.lo())/2
    else: return (f.hi(a) + f.lo(a))/2
def _up(f,v,a=None):
    assert f.geom is v.geom
    if a is None:
        if v.stag == f.stag:
            return (f*(v>0)).lo() + (f*(v<0)).hi()
        else:
            return f.lo()*(v>0) + f.hi()*(v<0)
    else:
        if v.stags[a] == f.stags[a]:
            return (f*(v>0)).lo(a) + (f*(v<0)).hi(a)
        else:
            return f.lo(a)*(v>0) + f.hi(a)*(v<0)

#%% With error checking, user-accessible
# Convention:
#   f is a node-field, u is a center-field
#   v is a center-velocity, w is a node-velocity
def c_dif(f,a=None):
    "Difference applied to node-field, result is center-field"
    if a is None:
        assert isinstance(f,ExtendedArray1D)
        assert f.stag == 0
        return _dif(f)
    else:
        assert isinstance(f,ExtendedArray2D)
        assert f.stags[a] == 0
        return _dif(f,a)
def n_dif(f,a=None):
    "Difference applied to center-field, result is node-field"
    if a is None:
        assert isinstance(f,ExtendedArray1D)
        assert f.stag == 1
        return _dif(f)
    else:
        assert isinstance(f,ExtendedArray2D)
        assert f.stags[a] == 1
        return _dif(f,a)
def c_ave(f,a=None):
    "Average applied to node-field, result is center-field"
    if a is None:
        assert isinstance(f,ExtendedArray1D)
        assert f.stag == 0
        return _ave(f)
    else:
        assert isinstance(f,ExtendedArray2D)
        assert f.stags[a] == 0
        return _ave(f,a)
def n_ave(f,a=None):
    "Average applied to center-field, result is node-field"
    if a is None:
        assert isinstance(f,ExtendedArray1D)
        assert f.stag == 1
        return _ave(f)
    else:
        assert isinstance(f,ExtendedArray2D)
        assert f.stags[a] == 1
        return _ave(f,a)
def c_upc(f,v,a=None):
    """Upwind value applied to node-field, with center-velocity.
    The result is center-field"""
    if a is None:
        assert isinstance(f,ExtendedArray1D) and isinstance(v,ExtendedArray1D)
        assert f.stag == 0 and v.stag == 1
        return _up(f,v)
    else:
        assert isinstance(f,ExtendedArray2D) and isinstance(v,ExtendedArray2D)
        assert f.stags[a] == 0 and v.stags[a] == 1 and v.stags[1-a]==f.stags[1-a]
        return _up(f,v,a)
def n_upc(u,v,a=None):
    """Upwind value applied to center-field, with center-velocity.
    The result is node-field"""
    if a is None:
        assert isinstance(u,ExtendedArray1D) and isinstance(v,ExtendedArray1D)
        assert u.stag == 1 and v.stag == 1
        return _up(u,v)
    else:
        assert isinstance(u,ExtendedArray2D) and isinstance(v,ExtendedArray2D)
        assert u.stags[a] == 1 and v.stags[a] == 1 and u.stags[1-a]==v.stags[1-a]
        return _up(u,v,a)
def c_upn(f,w,a=None):
    """Upwind value applied to node-field, with node-velocity.
    The result is center-field"""
    if a is None:
        assert isinstance(f,ExtendedArray1D) and isinstance(w,ExtendedArray1D)
        assert f.stag == 0 and w.stag == 0
        return _up(f,w)
    else:
        assert isinstance(f,ExtendedArray2D) and isinstance(w,ExtendedArray2D)
        assert f.stags[a] == 0 and w.stags[a] == 0 and w.stags[1-a]==f.stags[1-a]
        return _up(f,w,a)
def n_upn(u,w,a=None):
    """Upwind value applied to center-field, with node-velocity.
    The result is node-field"""
    if a is None:
        assert isinstance(u,ExtendedArray1D) and isinstance(w,ExtendedArray1D)
        assert u.stag == 1 and w.stag == 0
        return _up(u,w)
    else:
        assert isinstance(u,ExtendedArray2D) and isinstance(w,ExtendedArray2D)
        assert u.stags[a] == 1 and w.stags[a] == 0 and u.stags[1-a]==w.stags[1-a]
        return _up(u,w,a)

#%% A few more advanced routines, used in advection algorithms
def n_interp_upc(f,v,a):
    """Value interpolated upwind, for node-field and center-velocity
    See also c_up, n_up etc.
    v here is v*dt and has dimensions of length"""
    if a is None:
        assert isinstance(f,ExtendedArray1D) and isinstance(v,ExtendedArray1D)
        assert f.stag == 0 and v.stag == 1
        return f-n_upc(v*c_dif(f),v)
    else:
        assert isinstance(f,ExtendedArray2D) and isinstance(v,ExtendedArray2D)
        assert f.stags[a] == 0 and v.stags[a] == 1
        assert f.stags[1-a] == v.stags[1-a]
        return f-n_upc(v*c_dif(f,a),v,a)

def c_interp_upc(u,v,a=None):
    """Value interpolated upwind, for center-field and center-velocity
    See also c_up, n_up etc.
    v here is v*dt and has dimensions of length"""
    if a is None:
        assert isinstance(u,ExtendedArray1D) and isinstance(v,ExtendedArray1D)
        assert u.stag == 1 and v.stag == 1
        return u-v*c_upc(n_dif(u),v)
    else:
        assert isinstance(u,ExtendedArray2D) and isinstance(v,ExtendedArray2D)
        assert u.stags[a] == 1 and v.stags[a] == 1
        assert u.stags[1-a] == v.stags[1-a]
        return u-v*c_upc(n_dif(u,a),v,a)

def n_interp_upn(f,w,a=None):
    """Value interpolated upwind, for node-field and node-velocity
    See also c_up, n_up etc.
    v here is v*dt and has dimensions of length"""
    if a is None:
        assert isinstance(f,ExtendedArray1D) and isinstance(w,ExtendedArray1D)
        assert f.stag == 0 and w.stag == 0
        return f-w*n_upn(c_dif(f),w)
    else:
        assert isinstance(f,ExtendedArray2D) and isinstance(w,ExtendedArray2D)
        assert f.stags[a] == 0 and w.stags[a] == 0
        assert f.stags[1-a] == w.stags[1-a]
        return f-w*n_upn(c_dif(f,a),w,a)

def c_interp_upn(u,w,a):
    """Value interpolated upwind, for center-field and node-velocity
    See also c_up, n_up etc.
    v here is v*dt and has dimensions of length"""
    if a is None:
        assert isinstance(u,ExtendedArray1D) and isinstance(w,ExtendedArray1D)
        assert u.stag == 1 and w.stag == 0
        return u-c_upn(w*n_dif(u),w)
    else:
        assert isinstance(u,ExtendedArray2D) and isinstance(w,ExtendedArray2D)
        assert u.stags[a] == 0 and w.stags[a] == 1
        assert u.stags[1-a] == w.stags[1-a]
        return u-c_upn(w*n_dif(u,a),w,a)

#%% More useful stuff
def same_ea_structure(ea1,ea2):
    """Check if two ExtendedArray's are compatible geometrically
    and by the number of elements"""
    if isinstance(ea1,ExtendedArray1D):
        return isinstance(ea2,ExtendedArray1D) and ea1.geom is ea2.geom \
            and ea1.stag==ea2.stag and ea1.nl==ea2.nl and ea1.nu==ea2.nu
    elif isinstance(ea1,ExtendedArray2D):
        return isinstance(ea2,ExtendedArray2D) and ea1.geom is ea2.geom \
            and ea1.stags==ea2.stags and ea1.nls==ea2.nls and ea1.nus==ea2.nus

# Decorator
def extend(fun):
    """Make a function taking numpy arrays into a function which can also
    take ExtendedArray. Works for both 1D and 2D arrays."""
    def newfun(ea):
        if isinstance(ea,ExtendedArray):
            res = ea.copy()
            res.arr=fun(ea.arr)
            return res
        else:
            return fun(ea)
    return newfun

#%% Prepare
OVERRIDE_NUMPY_FUNCTIONS_CONTEXT_1D.enter()
OVERRIDE_NUMPY_FUNCTIONS_CONTEXT_2D.enter()

#%% Test it
if __name__ == '__main__':
    #%% Stage 1
    grid = Grid(n_cells = 5, delta = 0.2, start = 0.0)
    geom = Geometry1D(grid)
    f = ExtendedArray1D(geom,stag=1,nl=1,nu=1)
    f.alloc(np.float)
    f[:] = np.arange(1,6)
    f[-1]=10
    print(np.sin(f))
    g = f.view[:] # want to get rid of this slicing
    
    #%% Stage 2
    f[end+1]=-10
    try:
        f[end+2]
    except IndexError as e:
        print('Caught exception:',repr(e))
    # Copy constructor
    ff = ExtendedArray1D.copy(f)
    #%% Negative nl, nu
    g = ExtendedArray1D(geom,stag=1,nl=-1,nu=-1)
    g.alloc(np.float)
    try:
        g[0]
    except IndexError as e:
        print('Caught exception:',repr(e))
    #%% Fake extended arrays
    h = ExtendedArray1D(geom,stag=1,nl=1,nu=1)
    sy.SET_SPARSE(False)
    # Instead of allocation, do a fake
    h.alloc_symbolic('h')
    h[-1] = 28
    print('Symbolic Array h =\n',h)
    print('The main matrix h.arr.op.M =\n',h.arr.op.M)
    print('The constraint matrix h.arr.ref.cnst.op.M =\n',h.arr.ref.cnst.op.M)
    hr = ExtendedArray1D.copy(h)
    from custom_numpy import customize, decustomize
    hr.arr = customize(np.arange(hr.nt))
    
    #%% 2D Extended Arrays
    
    #%% Stage 1
    np.set_printoptions(threshold=1000)
    gridx = Grid(n_cells = 5, delta = 0.2, start = 0.0)
    gridy = Grid(n_cells = 10, delta = 0.2, start = 0.0)
    geom = Geometry2D(gridx,gridy)
    f = ExtendedArray2D(geom,stags=(1,1),nls=(1,1),nus=(1,1))
    f.alloc(np.float)
    f[:,:] = np.arange(50).reshape(5,10)
    f[-1,:]=10
    print(f)
    print(np.sin(f))
    #%% Stage 2
    f[end+1,:]=-10
    try:
        f[end+2,:]
    except IndexError as e:
        print('Caught exception:',repr(e))
    # Copy constructor
    ff = ExtendedArray2D.copy(f)
    #%% Negative nl, nu
    g = ExtendedArray2D(geom,stags=(1,1),nls=(-1,1),nus=(-1,1))
    g.alloc(np.float)
    try:
        g[0,:]
    except IndexError as e:
        print('Caught exception:',repr(e))
    #%% Symbpolic extended arrays
    gridy = Grid(n_cells = 2, delta = 0.2, start = 0.0)
    geom = Geometry2D(gridx,gridy)
    hn = ExtendedArray2D(geom,stags=(1,1),nls=(0,1),nus=(0,1))
    #hn.alloc(np.float)
    hn.arr=np.arange(np.prod(hn.nts)).reshape(hn.nts)
    sy.SET_SPARSE(True)
    hs = ExtendedArray2D(geom,stags=(1,1),nls=(0,1),nus=(0,1))
    hs.alloc_symbolic('h')
    hs[:,-1] = 28
    print('Symbolic Array hs =\n',hs)
    print('The main matrix hs.arr.op.M =\n',hs.arr.op.M)
    print('The constraint matrix hs.arr.ref.cnst.op.M =\n',hs.arr.ref.cnst.op.M)
    #%%
    hs.lo(0)
    n_dif(hs,0)
