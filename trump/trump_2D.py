#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 18 19:45:57 2017

2D cartesian and cylindrical coordinate operations.

Usage:
    import trump_2D as car
    car.set_2d_geometry('cartesian')
    from trump_2D import c_difx,c_dify,n_difx,n_dify
    # Rock on!
or:
    import trump_2D as cyl
    cyl.set_2d_geometry('cylindrical')
    from trump_2D import c_difr,c_difz,n_difr,n_difz
    # ... Set up geomr, geomz
    cyl.initialize_cyl(geomr,geomz)
    # Rock on!

@author: nle003
"""

#%% Basics
import pickle
import numpy as np
import scipy.interpolate
from trump import end, span, ExtendedArray1D, Field1D, \
    c_ave, n_ave, c_dif, n_dif, c_upc, c_upn, n_upc, n_upn,\
    Grid, Geometry2D, ExtendedArray2D, Field2D, Solver2D, grid_x, customize,\
    SET_SPARSE, c_interp_upc, n_interp_upn, c_interp_upn, n_interp_upc
from trump.extended_array import grid_ea_x

def c_difx(f): return c_dif(f,0)
def c_dify(f): return c_dif(f,1)
def n_difx(f): return n_dif(f,0)
def n_dify(f): return n_dif(f,1)
def n_cardiv(fx,fy): return n_dif(fx,0)+n_dif(fy,1)

def integrate1d_(f,xb):
    pass
def integrate2d_(f,xb=None,yb=None,is_cyl=False):
    """This could be used for cyl also -- see "False" statements in the code.
    xb(rb) and yb(zb) are either None or sorted ascending!"""
    g = f.geom
    x, y = g.xs
    xc, yc = g.xcs
    if xb is None:
        squeezex = True
        xb = np.array([x[0],x[end]])
    else:
        squeezex = False
    if yb is None:
        squeezey = True
        yb = np.array([y[0],y[end]])
    else:
        squeezey = False
    assert (np.diff(xb)>0).all() and (np.diff(yb)>0).all()
    # The next line looks so weird because xb,yb are regular arrays and x,y are ExtendedArray1D
    if not (xb[0]>=x[0] and xb[-1]<=x[end] and yb[0]>=y[0] and yb[-1]<=y[end]):
        print('xb[0]=',xb[0],', xb[-1]=',xb[-1],', x[0]=',x[0],', x[end]=',x[end])
        print('yb[0]=',yb[0],', yb[-1]=',yb[-1],', y[0]=',y[0],', y[end]=',y[end])
        raise Exception('invalid limits')
    # -- do not integrate outside the grid!
    # Indices of cells participating in integration
    ix1 = np.searchsorted(xc.arr,xb[0])-xc.nl
    ix2 = np.searchsorted(xc.arr,xb[-1])-xc.nl
    iy1 = np.searchsorted(yc.arr,yb[0])-yc.nl
    iy2 = np.searchsorted(yc.arr,yb[-1])-yc.nl
    cumf = np.zeros((ix2-ix1+2,iy2-iy1+2))
    if is_cyl:
        fi = f*g.ri
    else:
        fi = f
    cumf[1:,1:] = np.cumsum(np.cumsum(fi[ix1:ix2,iy1:iy2],axis=0),axis=1)
    # Watch the order! z,r instead of r,z due to FORTRAN/MATLAB conventions
    cumres = scipy.interpolate.interp2d(
            yc[iy1-1:iy2],xc[ix1-1:ix2],cumf,bounds_error=True)(yb,xb)
    res = np.diff(np.diff(cumres,axis=0),axis=1)
    if squeezey:
        res = res[:,0]
    if squeezex:
        res = res[0]
    dv = g.grids[0].delta*g.grids[1].delta
    if is_cyl:
        dv *= 2*np.pi
    return dv*res

class Geometry2D_car(Geometry2D):
    def __init__(self,grid1,grid2,nls,nus):
        super().__init__(grid1,grid2)
        self.gridx=self.grids[0]
        self.gridy=self.grids[1]
        self.calculate_xs(nls,nus)
        # Duplicate a few functions
    def c_difx(self,f):
        "Probably useless"
        assert f.geom is self
        return c_dif(f,0)
    def Div(self,fx,fy):
        assert fx.geom is self and fy.geom is self
        #return n_dif(fx,0) + n_dif(fy,1)
        return n_cardiv(fx,fy)
    def integrate(self,f,xb=None,yb=None):
        assert f.geom is self
        return integrate2d_(f,xb,yb)

class Geometry2D_cyl(Geometry2D):
    """
    We have the following conventions for extending to negative radial coordinate:
        ri.stags==(0,0) and rd.stags==(1,0)
        ri, rd are always > 0 (even for negative and zero r_orig = f.xe(0)), so it is safe to divide
        The scalar functions f:
            Symmetric around r_orig==0
        The vector functions (vr,vφ,vz):
            vz is symmetric around r_orig==0
            vr is anti-symmetric around r_orig==0
            vφ (rarely used) is also anti-symmetric around r_orig==0
    With these conventions:
    1. Gradient of a scalar field f is (ri, rd are not used):
        (grad f)_r = c_dif(f,0)
        (grad f)_z = c_dif(f,1)
    2. The curl of a polar field Ψ (i.e., with only φ-component ψ), such that ψ.stags==(1,1):
        (curl Ψ)_r = -n_dif(ψ,1) # stags=(1,0)
        (curl Ψ)_z =  n_dif(ψ*c_ave(rd,1),0)/c_ave(ri,1) # stags=(0,1)
    3. Divergence of a vector field (vr,vz) such that vr.stags==(1,0), vz.stags==(0,1):
        div v = n_dif(vr*rd,0)/ri + n_dif(vz,1)
    4. Average in r for a field with stags=(1,0):
        <f>_r = n_ave(f*rd,0)/ri
    The divergence of field with vr.stags=(0,x) and average <f>_r with f.stags=(0,x) are then
    also smoothly defined by switching ri <-> rd (and n_... <-> c_... along r) in the above formulas.
    One must check that rd and ri have to be in the formulas either none or both (never alone) and
    on opposite parts of a fraction.
    """
    def __init__(self,grid1,grid2,nls,nus):
        super().__init__(grid1,grid2)
        self.gridr=self.grids[0]
        self.gridz=self.grids[1]
        assert self.gridr.start==0 and nls[0]>=0 # the whole thing is useless unless
        self.calculate_xs(nls,nus)
        #self.r = grid_ea_x(self.gridr,stag=0,nl=nls[0],nu=nus[0]) # for various purposes
        ## We must have self.r[0]==0 for geometry to be valid
        #self.z = grid_ea_x(self.gridz,stag=0,nl=nls[1],nu=nus[1])
        ## Central points
        #self.zc = grid_ea_x(self.gridz,stag=1,nl=nls[1]+1,nu=nus[1]+1)
        #self.rc = grid_ea_x(self.gridr,stag=1,nl=nls[0]+1,nu=nus[0]+1)
        # rd, ri are used for differentiation and integration
        rc = self.xcs[0]
        r = self.xs[0]
        if False:
            # Motivation of how we obtain ri - but actual calculations are after "else"
            a = rc**2/2*np.sign(rc)
            # -- effective signed (!) area of a circle of radius xcs[0]
            ri1 = n_dif(a)
            # -- is always >0 and same as r except self.ri1[0]=dr/4
        else:
            ri1 = r.copy()
            ri1.arr = np.abs(r.arr)
            ri1[0]=self.gridr.delta/4
        self.ri = ExtendedArray2D(self,stags=(0,0),nls=nls,nus=nus)
        self.ri.arr = np.repeat(np.expand_dims(ri1.arr,axis=1),self.ri.nts[1],axis=1)
        # "Customization" is not necessary if in all symbolic operations r,rc is the second operand
        #r.arr = customize(np.repeat(r1.arr.reshape(r.nts[0],1),r.nts[1],axis=1))
        self.rd = ExtendedArray2D(self,stags=(1,0),nls=(nls[0]+1,nls[1]),nus=(nus[0]+1,nus[1]))
        self.rd.arr = np.repeat(np.abs(np.expand_dims(rc.arr,axis=1)),self.rd.nts[1],axis=1)
        # -- is >0
        #rc.arr = customize(np.repeat(np.abs(rc1.arr.reshape(rc.nts[0],1)),rc.nts[1],axis=1)) # is >0
        # r for integration:
        #self.ri = self.r.copy()
        #self.ri.arr = self.r.arr.copy()
        #self.ri[0,-nls[1]:end+nus[1]]=self.gridr.delta/8
    def Div(self,fr,fz):
        assert fr.geom is self and fz.geom is self
        return n_dif(fr*self.rd,0)/self.ri + n_dif(fz,1)
    def integrate(self,f,rb=None,zb=None):
        assert f.geom is self
        return 2*np.pi*integrate2d_(f*self.ri,rb,zb)

#%% Advection algorithms
def FaceValue_UTOPIA(f,vx,vy):
    "vx, vy have dimension of space, i.e. are actual velocity times dt"
    # The flux is fluxx=vxcx*fvx and fluxy=vycy*fvy
    # First-order face value
    assert isinstance(f.geom,Geometry2D) and vx.geom is vy.geom is f.geom
    g = f.geom
    if isinstance(g,Geometry2D_cyl):
        is_cyl=True
    elif isinstance(g,Geometry2D_car):
        is_cyl=False
    else:
        raise Exception('Unknown geometry type')
    if is_cyl:
        fv1 = [c_upc(f,vx,0)*g.rd,c_upc(f*g.ri,vy,1)]
    else:
        fv1 = [c_upc(f,vx,0),c_upc(f,vy,1)]
    # Third order face value
    # It is only function of dfx,dfy, no f
    if is_cyl:
        dfx=c_dif(f,0)*g.rd
        dfy=c_dif(f*g.ri,1)
    else:
        dfx=c_dif(f,0)
        dfy=c_dif(f,1)        
    # Weighted-averaged with velocity
    dfxw = n_interp_upc(dfx,c_ave(vy,0),1)
    dfyw = n_interp_upc(dfy,c_ave(vx,1),0)
    # Corrected dfx,dfy for 3rd-order method
    nvx = g.grids[0].delta * np.sign(vx)
    nvy = g.grids[1].delta * np.sign(vy)
    dfxc = c_interp_upc(dfxw,(nvx+vx)/3.,0)
    dfyc = c_interp_upc(dfyw,(nvy+vy)/3.,1)
    #dfv3 = [(nvx-vx)/2.*dfxc - c_upc(n_upc(vy*dfy,vy,1),vx,0)/2.,
    #        (nvy-vy)/2.*dfyc - c_upc(n_upc(vx*dfx,vx,0),vy,1)/2.]
    dfv3 = [[(nvx-vx)/2.*dfxc, -c_upc(n_upc(vy*dfy,vy,1),vx,0)/2.],
            [(nvy-vy)/2.*dfyc, -c_upc(n_upc(vx*dfx,vx,0),vy,1)/2.]]
    # Face value is
    # f_x=fv1[0]+dfv3[0][0]+dfv3[0][1] at (i-1/2,j)
    # and
    # f_y=fv1[1]+dfv3[1][0]+dfv3[1][1] at (i,j-1/2)
    return (fv1,dfv3)

# The flux correction algorithm of Leonard [1991]
def arrmin(f1,f2,s): return(f1+f2-s*np.abs(f1-f2))/2.
def arrmin3(f1,f2,f3,s): return arrmin(f1,arrmin(f2,f3,s),s)
def arrmax(f1,f2,s): return (f1+f2+s*np.abs(f1-f2))/2.
TVD_tmp = None
def TVD_ULTIMATE(f,vcr,fv1,dfv3,apply=True):
    "as usual, v has dimensions of length"
    assert isinstance(f.geom,Geometry2D) and vcr[0].geom is vcr[1].geom is f.geom
    if not apply:
        fluxl=[(fv1[0]+dfv3[0][0]+dfv3[0][1])*vcr[0],
               (fv1[1]+dfv3[1][0]+dfv3[1][1])*vcr[1]]
        return fluxl
    g = f.geom
    if isinstance(g,Geometry2D_cyl):
        is_cyl=True
    elif isinstance(g,Geometry2D_car):
        is_cyl=False
    else:
        raise Exception('Unknown geometry type')
    fluxl=[]
    divide_in_beginning = False # makes no difference, maybe False is more intuitive (recommended)
    for a in range(2):
        da = f.geom.grids[a].delta
        # All of the following variables have dimensions of f in cartesian or f*r in cylindrical
        # (except tmp,s which are dimensionless)
        #print('a=',a)
        # Leonard's [1991] phiC, phiD and phiU
        #fC = 0 # c_up(f,vcr[a],a) # same as fv1[a]
        if divide_in_beginning:
            if is_cyl:
                if a==0:
                    r = g.rd
                else:
                    r = c_ave(g.ri,1)
            else:
                r = 1
            dfa = c_dif(f,a)*da
        else:
            r = 1
            if is_cyl:
                if a==0:
                    dfa = c_dif(f,0)*g.rd*da
                if a==1:
                    dfa = c_dif(f*g.ri,1)*da
            else:
                dfa = c_dif(f,a)*da
        fv = dfv3[a][0]/r
        fD = np.sign(vcr[a])*dfa
        CURV = c_upc(n_dif(dfa,a),vcr[a],a)*da # same sign indep of v
        fU = CURV - fD
        DEL= fD - fU
        fref = fU.copy()
        fref.arr = fU.arr.copy()
        inz=(vcr[a]!=0)
        tmp = fU.copy()
        tmp.alloc(np.double)
        tmp.setv = np.abs(vcr[a])/da
        fref[inz] -= fU[inz]/tmp[inz]
        # Note that when velocity is zero, flux is zero and there is nothing
        # to limit.
        s=np.sign(DEL)
        fvnew = arrmax(arrmin3(fv,fref,fD,s),0,s)
        ic = (np.abs(CURV)>=np.abs(DEL))
        fvnew[ic] = 0
        fluxl.append(vcr[a]*(fv1[a] + fvnew*r + dfv3[a][1]))
    return fluxl

# Various algorithms for advection
def AdvectionStepI(f,vx,vy,alg=None):
    "vx, vy have dimensions of space, i.e., are actual velocity times dt"
    assert isinstance(f.geom,Geometry2D) and vx.geom is vy.geom is f.geom
    g = f.geom
    if (vx.arr>g.grids[0].delta).any() or (vy.arr>g.grids[1].delta).any():
        print('WARNING: CFL condition is not satisfied in AdvectionStepI')
    cyl_flux_methods = ['CIR','UTOPIA','ULTIMATE']
    if alg in cyl_flux_methods:
        fv1,dfv3=FaceValue_UTOPIA(f,vx,vy) # in cylindrical, these are facevalue x radius
    if alg=='UTOPIA' or alg=='ULTIMATE':
        fluxx,fluxy = TVD_ULTIMATE(f,(vx,vy),fv1,dfv3,apply=(alg=='ULTIMATE'))
    if alg=='CIR':
        # First-order upstream method [Courant, Isaacson and Rees, 1952]
        df = -n_cardiv(vx*fv1[0],vy*fv1[1]) # is required df x radius
    elif alg=='MacCormack_Reverse':
        # Stable, 2nd order
        # Even more stable than Lax-Wendroff
        # Slightly better than MacCormack?? About the same
        # 1. Calculate interim value
        fi = f - g.Div(vx*c_upc(f,-vx,0),vy*c_upc(f,-vy,1))
        df = (-f+fi)/2 - g.Div(vx*c_upc(fi,vx,0),vy*c_upc(fi,vy,1))/2.
    elif alg=='MacCormack':
        # Stable, 2nd order
        # 1. Calculate interim value
        fi = f - g.Div(vx*c_upc(f,vx,0),vy*c_upc(f,vy,1))
        df = (-f+fi)/2 - g.Div(vx*c_upc(fi,-vx,0),vy*c_upc(fi,-vy,1))/2.
    elif alg=='UTOPIA' or alg=='ULTIMATE':
        # Third-order upstream method, without or with flux correction
        df = -n_cardiv(fluxx,fluxy)
    else:
        raise Exception('Unknown advection algorithm: '+alg)
    if isinstance(g,Geometry2D_car):
        return df
    elif isinstance(g,Geometry2D_cyl):
        # First of all, make sure we have the axis
        if not df.nls[0]>=0:
            raise Exception('AdvectionStepI: df is not extended to axis. Possible cure:\n'+\
                            '\tincrease nls[0] of '+f.name+', vr, vz or r/rc')
        if alg in cyl_flux_methods:
            return df/g.ri
        else: # MacCormack methods
            return df
    else:
        raise Exception('Unknown geometry type')

####################################################################################################
#%% A bunch of plotting routines
import matplotlib as mpl
import matplotlib.pyplot as plt

def ea_plot(ea,scale=1,plotargs=(),plotopts={}):
    arr = np.double(ea.arr)
    if isinstance(ea,ExtendedArray1D):
        plt.plot(ea.xe/scale,arr,*plotargs,**plotopts)
    elif isinstance(ea,ExtendedArray2D):
        plt.pcolormesh(ea.pcolor_xe(0)/scale,ea.pcolor_xe(1)/scale,arr.T,*plotargs,**plotopts)
        a=plt.gca()
        a.set_aspect('equal')
        plt.colorbar()
        
def ea_plot_slice(ea,ir=None,iz=None,scale=1,plotargs=(),plotopts={}):
    if ir is None:
        if iz is None: raise Exception('bla!')
        # Plot alont r
        plt.plot(ea.xe(0)/scale,ea[span,iz],*plotargs,**plotopts)
    else:
        if iz is not None: raise Exception('bla?')
        plt.plot(ea.xe(1)/scale,ea[ir,span].T,*plotargs,**plotopts)

def save_figure(fig,savedir,savefmts,kplot):
    "Independent of what kind of figure we are saving"
    fig.canvas.draw()
    if savedir != None:
        for savefmt in savefmts:
            figfname = savedir+'/img{:05d}.'.format(kplot)+savefmt
            if savefmt=='pkl':
                with open(figfname,'wb') as f:
                    pickle.dump(fig,f)
            else:
                fig.savefig(figfname)
    
# A useful class for pcolor plots
class UpdateablePcolor:
    "Combined for cartesian and cylindrical"
    def __init__(self,fignum=None,zmin=None,zmax=None,savedir=None,savefmts=['png'],
                 figsize=None,scale=1,cmap=mpl.cm.viridis):
        self.fignum=fignum
        self.started_plotting=False
        self.zmin0=zmin
        self.zmax0=zmax
        self.savefmts=savefmts
        self.savedir=savedir
        if self.savedir is not None:
            if isinstance(savefmts,list):
                self.savefmts=savefmts
            else:
                # Only one
                self.savefmts=[savefmts]
        self.figsize=figsize
        self.scale=scale
        self.cmap=cmap
    def plot(self,ea_orig,title=''):
        if not self.started_plotting:
            self.fig = plt.figure(self.fignum,figsize=self.figsize)
            self.fignum = self.fig.number
            print('Figure',self.fignum,': savedir =',self.savedir)
            self.fig.clear()
            self.ax=self.fig.gca()
            # The first plot
            # ax.set_position([.1,.1,(Lx/Ly)*0.8,0.8])
            g = ea_orig.geom
            if isinstance(g,Geometry2D_cyl):
                # Cylindrical
                self.is_cyl=True
                assert ea_orig.geom.grids[0].start==0 # geometry in r-direction must start at axis
                assert ea_orig.nls[0]>=0 # cannot miss the axis
                self.mlx=0
            elif isinstance(g,Geometry2D_car):
                self.is_cyl=False
                self.mlx=max(-ea_orig.nls[0],0)
            else:
                raise Exception('Unknown geometry type')
            # Meaningful values
            self.mux=max(-ea_orig.nus[0],0)
            self.mly=max(-ea_orig.nls[1],0)
            self.muy=max(-ea_orig.nus[1],0)
        ea = ea_orig.view[self.mlx:end-self.mux,self.mly:end-self.muy]
        if self.is_cyl:
            if ea.stags[0]==0:
                fplot = np.vstack((ea.arr[::-1,:],ea.arr[1:,:]))
            else:
                fplot = np.vstack((ea.arr[::-1,:],ea.arr))
        else:
            fplot=ea.arr
        if not self.started_plotting:
            x = ea.pcolor_xe(0)/self.scale
            if self.is_cyl:
                if ea.stags[0]==0:
                    self.xplot=np.hstack((-x[::-1],x[2:]))
                else:
                    self.xplot=np.hstack((-x[::-1],x[1:]))
            else:
                self.xplot=x
            self.yplot=ea.pcolor_xe(1)/self.scale
            self.pcolor = self.ax.pcolorfast(self.xplot,self.yplot,fplot.T,cmap=self.cmap)
            self.ax.set_aspect('equal')
            self.ax.set_xlim(np.min(self.xplot),np.max(self.xplot))
            self.ax.set_ylim(np.min(self.yplot),np.max(self.yplot))
            self.sm = mpl.cm.ScalarMappable(cmap=self.cmap)
            self.sm.set_array(np.linspace(0,1,128))
            self.kplot=0
        else:
            # Replacement plot
            self.pcolor.set_data(fplot.T)
        if self.zmin0==None:
            zmin=np.min(fplot)
        else:
            zmin=self.zmin0
        if self.zmax0==None:
            zmax=np.max(fplot)
        else:
            zmax=self.zmax0
        self.pcolor.set_clim(zmin,zmax)
        self.sm.set_clim(zmin,zmax)
        if not self.started_plotting:
            self.fig.colorbar(self.sm)
            self.started_plotting=True
        self.ax.set_title(title)
        self.save()
        self.kplot += 1
    def save(self):
        save_figure(self.fig,self.savedir,self.savefmts,self.kplot)
    def wait(self):
        self.fig.waitforbuttonpress() # progress by clicking mouse

from mpl_toolkits.axes_grid1 import make_axes_locatable
class UpdateablePcolorAxis:
    "Combined for cartesian and cylindrical"
    def __init__(self,ax,zmin=None,zmax=None,scale=1,pseudo=False,cmap=mpl.cm.viridis,zoom=None):
        self.ax=ax
        self.started_plotting = False
        self.update_axes = True
        self.zmin0=zmin
        self.zmax0=zmax
        self.scale=scale
        self.cmap=cmap
        self.pseudo=pseudo
        self.zoom=zoom
    def plot(self,ea_orig,title=''):
        if self.update_axes:
            # The first plot
            # ax.set_position([.1,.1,(Lx/Ly)*0.8,0.8])
            g=ea_orig.geom
            if isinstance(g,Geometry2D_cyl):
                self.is_cyl=True
                # Cylindrical
                assert g.grids[0].start==0 # geometry in r-direction must start at axis
                assert ea_orig.nls[0]>=0 # cannot miss the axis
                self.mlx=0
            elif isinstance(g,Geometry2D_car):
                self.is_cyl=False
                self.mlx=max(-ea_orig.nls[0],0)
            # Meaningful values
            self.mux=max(-ea_orig.nus[0],0)
            self.mly=max(-ea_orig.nls[1],0)
            self.muy=max(-ea_orig.nus[1],0)
        ea = ea_orig.view[self.mlx:end-self.mux,self.mly:end-self.muy]
        if self.is_cyl:
            if ea.stags[0]==0:
                fplot = np.vstack((ea.arr[::-1,:]*(-1 if self.pseudo else 1),ea.arr[1:,:]))
            else:
                fplot = np.vstack((ea.arr[::-1,:]*(-1 if self.pseudo else 1),ea.arr))
        else:
            fplot=ea.arr
        if self.update_axes:
            x = ea.pcolor_xe(0)/self.scale
            if self.is_cyl:
                if ea.stags[0]==0:
                    self.xplot=np.hstack((-x[::-1],x[2:]))
                else:
                    self.xplot=np.hstack((-x[::-1],x[1:]))
            else:
                self.xplot=x
            self.yplot=ea.pcolor_xe(1)/self.scale
            self.pcolor = self.ax.pcolorfast(self.xplot,self.yplot,fplot.T,cmap=self.cmap)
            self.ax.set_aspect('equal')
            if self.zoom is None:
                self.ax.set_xlim(np.min(self.xplot),np.max(self.xplot))
                self.ax.set_ylim(np.min(self.yplot),np.max(self.yplot))
            else:
                self.ax.set_xlim(self.zoom[0][0]/self.scale,self.zoom[0][1]/self.scale)
                self.ax.set_ylim(self.zoom[1][0]/self.scale,self.zoom[1][1]/self.scale)
        else:
            # Replacement plot
            self.pcolor.set_data(fplot.T)
        if self.zmin0==None:
            zmin=np.min(fplot)
        else:
            zmin=self.zmin0
        if self.zmax0==None:
            zmax=np.max(fplot)
        else:
            zmax=self.zmax0
        self.pcolor.set_clim(zmin,zmax)
        if not self.started_plotting:
            self.sm = mpl.cm.ScalarMappable(cmap=self.cmap)
            self.sm.set_array(np.linspace(0,1,128))            
        self.sm.set_clim(zmin,zmax)
        if not self.started_plotting:
            divider = make_axes_locatable(self.ax)
            self.cax = divider.append_axes('right', size='5%', pad=0.05)
            self.ax.figure.colorbar(self.sm, ax=self.ax, cax=self.cax, orientation='vertical')
            #self.ax.colorbar(self.sm)
            self.started_plotting=True
        self.ax.set_title(title)
        self.update_axes = False
    pass

class UpdateablePcolorSubplots:
    "Combined for cartesian and cylindrical"
    def __init__(self,nrows,ncols,num=None,zmin=None,zmax=None,savedir=None,savefmts=['png'],
                 figsize=None,pseudos=None,scale=1,cmap=mpl.cm.viridis,zoom=None):
        nsubplots=nrows*ncols
        self.savedir=savedir
        if isinstance(savefmts,list):
            self.savefmts=savefmts
        else:
            self.savefmts=[savefmts]
        plt.close(num)
        #fig=plt.figure(num,figsize=figsize)
        #fig.clear()
        self.fig,axs = plt.subplots(nrows,ncols,num=num,figsize=figsize)
        self.fig.tight_layout()
        axs.resize(nsubplots)
        self.uaxs=np.array([UpdateablePcolorAxis(axs[k],scale=scale,zmin=zmin,zmax=zmax,
            cmap=cmap,pseudo=pseudos[k],zoom=zoom) for k in range(nsubplots)])
        self.fignum = self.fig.number
    def info(self):
        print('Figure',self.fignum,': savedir =',self.savedir)
    def subplot(self,subnum,*arg,**kw):
        self.uaxs[subnum-1].plot(*arg,**kw)
    def save(self,kplot):
        save_figure(self.fig,self.savedir,self.savefmts,kplot)
    def close(self):
        plt.close(self.fignum)
    def wait(self):
        self.fig.waitforbuttonpress() # progress by clicking mouse

