#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Change log:

Nov 30, 2016:
Vertion 4 of API:
    Removed UpdateField1D class, user must update BC himself by "apply_bc()"

Nov 29, 2016:
Version 3 of API:
    Moved Temp1D to another file extended_array.py, changed the indexing to a
    more intuitive. God rid of dependent/independent tracking on top level (all
    done in a symbolic array)

Oct 27, 2016:
Version 2 of API:
    1. Move dif, ave and up functions to user space
    2. Instead of nl, nu track "dependent" and "independent" compononents
    3. Move BC setting also to user space. Generalize it to setting constraints
    4. Old version saved as field1d_api1.py

Created on Thu Oct 13 20:00:56 2016

@author: nle003
"""
#%% Imports
import sys
import io
import numpy as np
from numbers import Integral
from collections import namedtuple
import time
#import functools
#import operator

import trump
import trump.symbolic_array as sy
from trump.symbolic_array import SymbolicArray
import trump.extended_array as ea
from trump.extended_array import ExtendedArray1D, Grid, end, ExtendedArray2D,\
    c_dif, n_dif, c_ave, n_ave, c_upc, n_upc, same_ea_structure
from trump.custom_ndarray import customize, decustomize

#%% Auxiliary: the BC manager - names constraints and can update variable constraints
class BCManager(sy.ConstraintManager):
    def __init__(self,field):
        self.field = field
        self.cnst = field.symb.arr.ref.cnst
        # Auxiliary information about complex constraints
        self.start = np.zeros((0,),dtype=np.int)
        self.is_variable = np.zeros((0,),dtype=np.bool)
        self.names = []
        # Take over as the constraint manager
        self.saved_manager = self.field.symb.arr.cnst_manager # will not be ever restored
        self.field.symb.arr.cnst_manager = self
        self._frozen = False
        self.current_name = None # for all constraints
        self.default_constraint_number = 0
        self.updating_k = None # only for updating variable constraints
        self.block_setitem = True # Do not allow setitem outside of this manager
    def record(self,name,is_variable=False):
        assert not self._frozen
        self.end_update() # cleanup from the previous
        try:
            k = self.names.index(name)
        except ValueError:
            pass
        else:
            if self.is_variable[k]:
                raise Exception('Use "update" for variable BC '+name)
            else:
                raise Exception('There is already BC called '+name)
        self.names.append(name)
        self.current_name = name
        self.is_variable = np.hstack((self.is_variable,is_variable))
        self.start = np.hstack((self.start,self.cnst.op.M.shape[0]))
    def update(self,name):
        self.end_update() # cleanup from the previous
        k = self.names.index(name)
        assert self.is_variable[k]
        self.updating_k = k
        self.tmp_op = sy.Oper(M=np.ndarray((0,self.cnst.n)),V=np.ndarray((0,)))
        if self.field.symb.arr.ref.array_like:
            self.tmp_ind = np.ndarray((0,),dtype=np.int)
        else:
            self.tmp_ind = None
    def __getitem__(self,i):
        # Just forward, no extra work
        return self.field.symb[i]
    def workon(self,c,ind):
        "Called by __setitem__ in a symbolic array"
        if self.block_setitem:
            raise Exception('Please use bc manager for setting constraints')
        if self.updating_k is None:
            # Usual situation, just do the default thing
            assert not self.current_name is None
            self.cnst.append(c,ind)
        else:
            # Updating a variable constraint
            self.tmp_op = sy.oper_cat(self.tmp_op,c)
            if self.field.symb.arr.ref.array_like and ind is None:
                raise Exception('internal error')
            else:
                self.tmp_ind = np.hstack((self.tmp_ind,ind))
    def __setitem__(self,i,v):
        # Forward to self.field.symb, this will trigger workon anyway
        self.block_setitem = False
        if self.current_name is None:
            # Recording a nameless non-variable bc
            name='_default_bc_#'+str(self.default_constraint_number)
            self.default_constraint_number += 1
            self.record(name,is_variable=False)
        self.field.symb[i]=v # triggers a call to "self.workon"
        self.block_setitem = True
    def coords(self,k):
        "Start and end of constraint #k"
        start = self.start[k]
        if k == len(self.is_variable) - 1:
            stop = self.num
        else:
            stop = self.start[k+1]
        return slice(start,stop)        
    def end_update(self):
        "Cleanup after previous variable bc recording"
        if not self.updating_k is None:
            # do the cleanup
            k = self.updating_k
            i = self.coords(k)
            if not sy.array_equal(self.tmp_op.M,sy.to_indexable(self.cnst.op.M)[i,:]):
                #print_constraints(self,'f')
                # No recovery, must re-do the whole thing
                raise Exception('The updated BC "'+self.current_name+'" must be the same!')
            assert sy.array_equal(sy.to_indexable(self.cnst.op.M)[i,:],self.tmp_op.M)
            # Not necessary to determine ind, they are the same
            if not self.tmp_ind is None:
                assert np.array_equal(self.tmp_ind,self.cnst.ind[i])
            self.cnst.op.V[i]=self.tmp_op.V
            # switch back the manager
            #assert not self.saved_manager is None
            #self.field.symb.arr.cnst_manager = self.saved_manager
            #self.saved_manager = None
            self.updating_k = None
        # There is no need to cleanup for a regular bc recording
    def freeze(self):
        "Before applying the constraints, make sure they are final"
        if self.frozen:
            print('WARNING: re-freezing')
        t0 = time.time()
        self.end_update() # cleanup from the previous update
        self._frozen = True
        if self.num==0:
            return
        if trump.DEBUGLEVEL>1: t1 = time.time()
        M = sy.to_indexable(self.cnst.op.M)
        if trump.DEBUGLEVEL>1: t2 = time.time()
        self.Md = sy.to_solvable(M[:,self.cnst.dep])
        if trump.DEBUGLEVEL>1: t3 = time.time()
        self.Mi = sy.to_solvable(M[:,~self.cnst.dep])
        if trump.DEBUGLEVEL>1: t4 = time.time()
        # Check for conflicting/repeating BC:
        assert self.Md.shape[0]==self.Md.shape[1]
        if trump.DEBUGLEVEL>0:
            if np.linalg.matrix_rank(self.Md.A)<self.Md.shape[0]:
                raise Exception('Conflicting or repeating BC in field '+self.field.name)
        if trump.DEBUGLEVEL>1:
            t5 = time.time()
            print('Freezing BC for',self.field.name,', times: clean =',(t1-t0),': index =',(t2-t1),
                  '; dep =',(t3-t2),'; indep =',(t4-t3),'; check =',(t5-t4))
    @property
    def num(self):
        return self.cnst.op.M.shape[0]
    @property
    def frozen(self):
        return self.num==0 or self._frozen
    def apply(self):
        self.end_update()
        assert self.frozen
        if self.field.trace:
            unset = ((~self.field.touched) & self.field.indep)
            if unset.any():
                s = self.field.name + '.bc.apply():\n\t'
                s += 'The following elements had not been updated before applying BC:\n\t'
                ii=np.where(unset)[0]
                s += '['
                s += ', '.join(str(self.field.restore_index(i)) for i in ii)
                s += ']'
                raise Exception(s)
        if self.num > 0:
            dep = self.cnst.dep
            x = self.field.arr.flat
            x[dep] = -sy.solve(self.Md,self.cnst.op.V+self.Mi.dot(x[~dep]))
        if self.field.trace:
            self.field.touched[:]=False
    def check(self):
        "Check if BC are satisfied numerically. Returns error (0 if satisfied)."
        assert self.frozen
        if self.num>0:
            x = self.field.arr.flat
            return np.max(np.abs(sy.oper_apply(self.cnst.op,x)))
        else:
            return 0
    def print(self,file=sys.stdout):
        "Pretty print of BC's"
        # This function is ugly, must clean up
        name = self.field.symb.arr.ref.name
        print('Boundary conditions for symbolic array',name,file=file)
        def arg_str(x,mid=False):
            if x==0:
                return '' if mid else '0'
            return (' %+f ' if mid else '%f ') % (x,)
        def coef_str(x,ss,mid=False):
            if x==0:
                return ''
            if x == 1:
                s = ' + ' if mid else ''
            elif x==-1:
                s = ' - '
            else:
                s = ('%+f * ' if mid else '%f * ') % (x,)
            return s+ss
        def index_str(rii):
            if isinstance(rii,tuple):
                ris = '%d,%d' % rii
            else:
                ris = '%d' % (rii,)
            return ris
        for kc in range(len(self.is_variable)):
            isvar = self.is_variable[kc]
            print('BC #',kc,': ','variable' if isvar else 'nonvariable',', name="',
                  self.names[kc],'"',sep='',file=file)
            i = self.coords(kc)
            op = sy.oper_getitem(self.cnst.op,i)
            ind = self.cnst.ind[i]
            M = sy.to_dense( op.M.copy())
            n = M.shape[0]
            for k in range(n):
                ris = index_str(self.field.symb.restore_index(ind[k]))
                Mrow = M[k,:]
                mainc = Mrow[ind[k]]
                Mrow[ind[k]]=0
                print('\t%s = ' % (coef_str(mainc,'%s[%s]' % (name,ris),False)),
                      end='',file=file)
                ii = np.where(Mrow)[0]
                mid = False
                for jj in ii:
                    rjs = index_str(self.field.symb.restore_index(jj))
                    print('%s' % (coef_str(-Mrow[jj],'%s[%s]' % (name,rjs),mid)),
                          end='',file=file)
                    mid = True
                print('%s' % arg_str(-op.V[k],mid),file=file)
    def __repr__(self):
        sio=io.StringIO()
        self.print(file=sio)
        s=sio.getvalue()
        sio.close()
        return s
    pass

#%% Extended array with boundary conditions and symbolic operations
class Field1D(ExtendedArray1D):
    """Same as ExtendedArray1D but with a name and boundary conditions enforced"""
    trace_all = False
    def __init__(self,name,geom,stag,nl,nu,trace=None):
        """See ExtendedArray1D.__init__ for most of the arguments. The only new one
        is "name" - the name of the field."""
        if trace is None:
            trace=self.__class__.trace_all
        super().__init__(geom,stag,nl=nl,nu=nu,trace=trace)
        self.alloc(np.float)
        #self.name = name
        # To store the bc and to record the symbolic operations
        self.symb = ExtendedArray1D.copy(self)
        self.symb.alloc_symbolic(name)
        self.bc = BCManager(self)
        # Dependent values
        self.dep = self.copy()
        # NOTE: this is a link, not a copy
        self.dep.arr=self.symb.arr.ref.cnst.dep
    @property
    def name(self):
        return self.symb.arr.ref.name
    @property
    def indep(self):
        "Mostly just a forwarder"
        raise Exception('OBSOLETE! Use Field1D.dep')
        return ~self.symb.arr.ref.cnst.dep
    def __repr__(self):
        return 'Field1D '+self.info(name=self.name)
    pass

#%% Class that does not record BC but is responsible for solving for some other
class Solver1D:
    """We skip some equations, default - the ones that are dependent"""
    def __init__(self,lhs,ea1_op,ea1_skip=None):
        "ea1_op is the symbolic operator, an ExtendedArray1D with a symbolic arr"
        assert isinstance(ea1_op,ExtendedArray1D) and ea1_op.is_symb
        assert isinstance(lhs,Field1D)
        assert(lhs.symb.arr.ref is ea1_op.arr.ref)
        self.symb_op = ea1_op.arr # need it because bc may change
        assert(lhs.bc.frozen)
        #self.lhs = lhs
        self.unknown_info=lhs.copy()  # shallow copy, just info
        self.rhs_info = ea1_op.copy() # shallow copy, just info
        if ea1_skip is None:
            skip = None
        else:
            assert isinstance(ea1_skip,ExtendedArray1D) and same_ea_structure(self.rhs_info,ea1_skip)
            skip = ea1_skip.arr
        equations = self.symb_op.op if skip is None else sy.oper_getitem(self.symb_op.op,~skip)
        self.skip = skip # save for future use
        self.padding = np.zeros((lhs.bc.num,))
        self.op_full = sy.oper_cat(equations,self.symb_op.ref.cnst.op)
        if not self.op_full.M.shape[0]==self.op_full.M.shape[1]:
            raise ValueError('# equations = '+str(self.op_full.M.shape[0])+\
                             ' is not the same as # variables = '+str(self.op_full.M.shape[1])+\
                             ' in solver for '+self.symb_op.ref.name)
        self.n_rhs = self.op_full.M.shape[0] - lhs.bc.num
    def solve_full(self,lhs,rhs_ea1):
        "With BC"
        lhs.bc.end_update() # cleanup the bc
        assert isinstance(rhs_ea1,ExtendedArray1D)
        assert same_ea_structure(self.rhs_info,rhs_ea1)
        #assert rhs_ea1.arr.size == self.n_rhs
        tmp = self.symb_op.ref.cnst.op.V
        if any(lhs.bc.is_variable):
            self.op_full.V[-len(tmp):]=tmp
        real_rhs = rhs_ea1.arr if self.skip is None else rhs_ea1.arr[~self.skip]
        lhs.arr[:] = sy.oper_solve(self.op_full,np.hstack((real_rhs,self.padding)))
    def print(self,file=sys.stdout):
        print('Solver for the unknown',self.symb_op.ref.name,'which is an',\
              self.unknown_info,file=file)
        print('\twith RHS as an',self.rhs_info,file=file)
        print('\tThere are',self.n_rhs,'independent variables',file=file)
        print('\twhich includes',self.op_full.M.shape[0],'total variables minus',
              len(self.padding),'linear constraints',file=file)
    def __repr__(self):
        sio=io.StringIO()
        self.print(file=sio)
        s=sio.getvalue()
        sio.close()
        return s        
    pass

#%% Extended array with boundary conditions and symbolic operations
class Field2D(ExtendedArray2D):
    """Same as ExtendedArray2D but with a name and boundary conditions enforced"""
    def __init__(self,name,geoms,stags,nls,nus,use_array=None):
        """See ExtendedArray2D.__init__ for most of the arguments. The only new one
        is "name" - the name of the field."""
        super().__init__(geoms,stags,nls=nls,nus=nus)
        if use_array is None:
            self.alloc(np.float)
        else:
            self.arr = use_array
        #self.name = name
        # To store the bc and to record the symbolic operations
        self.symb = ExtendedArray2D.copy(self)
        self.symb.alloc_symbolic(name)
        self.bc = BCManager(self)
        # Dependent values
        self.dep = self.copy()
        # NOTE: this is a link, not a copy
        self.dep.arr=self.symb.arr.ref.cnst.dep.reshape(self.nts[0],self.nts[1])
    @property
    def name(self):
        return self.symb.arr.ref.name
    @property
    def indep(self):
        "Mostly just a forwarder"
        raise Exception('OBSOLETE! Use Field2D.dep')
        return ~self.symb.arr.ref.cnst.dep
    def __repr__(self):
        return 'Field2D '+self.info(name=self.name)
    pass

#%% Class that does not record BC but is responsible for solving for some other
class Solver2D:
    def __init__(self,lhs,ea2_op,ea2_skip=None):
        "ea1_op is the symbolic operator, an ExtendedArray1D with a symbolic arr"
        assert isinstance(ea2_op,ExtendedArray2D) and ea2_op.is_symb
        assert isinstance(lhs,Field2D)
        assert(lhs.symb.arr.ref is ea2_op.arr.ref)
        #super().__init__(ea1_op.geom,ea1_op.stag,ea1_op.nl,ea1_op.nu)
        #oper = ea1_op.arr
        self.symb_op = ea2_op.arr
        assert(lhs.bc.frozen)
        #self.lhs = lhs
        self.unknown_info=lhs.copy()  # shallow copy, just info
        self.rhs_info = ea2_op.copy() # shallow copy, just info
        if ea2_skip is None:
            skip = None
        else:
            assert isinstance(ea2_skip,ExtendedArray2D) and same_ea_structure(self.rhs_info,ea2_skip)
            skip = ea2_skip.arr
        equations = self.symb_op.op if skip is None \
            else sy.oper_getitem(self.symb_op.op,~skip.flatten())
        self.skip = skip # save for future use
        self.padding = np.zeros((lhs.bc.num,))
        self.op_full = sy.oper_cat(equations,self.symb_op.ref.cnst.op)
        if not self.op_full.M.shape[0]==self.op_full.M.shape[1]:
            raise ValueError('# equations = '+str(self.op_full.M.shape[0])+\
                             ' is not the same as # variables = '+str(self.op_full.M.shape[1])+\
                             ' in solver for '+self.symb_op.ref.name)
        self.n_rhs = self.op_full.M.shape[0] - lhs.bc.num
    def solve_full(self,lhs,rhs_ea2):
        "With BC"
        lhs.bc.end_update()
        assert same_ea_structure(self.rhs_info,rhs_ea2)
        #assert rhs_ea2.arr.size == self.n_rhs
        tmp = self.symb_op.ref.cnst.op.V
        if any(lhs.bc.is_variable):
            self.op_full.V[-len(tmp):]=tmp
        real_rhs = rhs_ea2.arr.flat if self.skip is None else rhs_ea2.arr[~self.skip]
        lhs.arr[:,:] = sy.oper_solve(
            self.op_full,np.hstack((real_rhs,self.padding))).reshape(lhs.nts)
    def print(self,file=sys.stdout):
        print('Solver for the unknown',self.symb_op.ref.name,'which is an',\
              self.unknown_info,file=file)
        print('\twith RHS as an',self.rhs_info,file=file)
        print('\tThere are',self.n_rhs,'independent variables',file=file)
        print('\twhich includes',self.op_full.M.shape[0],'total variables minus',
              len(self.padding),'linear constraints',file=file)
    def __repr__(self):
        sio=io.StringIO()
        self.print(file=sio)
        s=sio.getvalue()
        sio.close()
        return s        
    pass

#%%
if __name__=='__main__':
    #%%
    trump.DEBUGLEVEL=2
    sy.SET_SPARSE(False)

    #%% First, test the FakeArray class
    print('\n\n********** TEST FA **********\n')
    f = SymbolicArray('f',5)
    # A trick to make operations like v * f and v + f work with ndarray v and
    # arbitrary class f (NOTE: f * v and f + v work fine even without it)
    v = customize(np.arange(1,6))
    mul = v*f
    mul_op = mul/f
    print('mul_op =',mul_op,'\n')
    add = v + f
    add_op = add/f
    print('add_op =',add_op,'\n')
    sub1 = v - f
    sub1_op = sub1/f
    print('sub1_op =',sub1_op,'\n')
    sub2 = f - v
    sub2_op = sub2/f
    print('sub2_op =',sub2_op,'\n')
    div = f / v
    div_op = div/f
    print('div_op =',div_op,'\n')

    #%%
    #fa.SET_SPARSE(False)
    geom = Grid(n_cells=5,delta=0.2,start=0)
    f = Field1D('f',geom,stag=0,nl=0,nu=0)
    f.bc.record('end')
    f.bc[end]=1
    f.bc.record('zero',is_variable=True)
    f.bc[0]=1
    f.bc.freeze()
    f[:] = np.random.rand(f.n)
    f.bc.apply()
    print(f.arr)
    f.bc.update('zero')
    f.bc[0]=10
    f.bc.apply()
    print(f.arr)
    f.bc.print()
    
    #%% Shortened geometry
    geomshort = ea.geom_extend(geom,-1,-1)
    area = ExtendedArray1D(geomshort,stag=0,nl=1,nu=1)
    curv = 7
    area.arr=customize(np.exp(curv*area.xe))
    areac = ExtendedArray1D(geomshort,stag=1,nl=1,nu=1)
    areac.arr=customize(np.exp(curv*areac.xe))
    
    #%% Fake field
    phi = Field1D('phi',geomshort,stag=0,nl=1,nu=1)
    phi.bc[-1]=0
    phi.bc.record('end',is_variable=True)
    phi.bc[end+1]=0
    phi.bc.freeze()
    
    lapl = -n_dif(areac*c_dif(phi.symb))/area
    gop = lapl.arr/phi.symb.arr
    op1 = sy.gen_op_total(gop)
    op = sy.gen_op_simplify(gop)
    print(op)
    
    #%% Fake field 2
    area = Field1D('area',geom,stag=0,nl=0,nu=0)
    curv = 7
    area.arr=customize(np.exp(curv*area.xe))
    areac = Field1D('areac',geom,stag=1,nl=0,nu=0)
    areac.arr=customize(np.exp(curv*areac.xe))
    
    rho = Field1D('rho',geom,stag=0,nl=-1,nu=-1)
    rho.arr=np.array([1,2,3,4])
    phi = Field1D('phi',geom,stag=0,nl=0,nu=0)
    phi.bc[end] = 0
    phi.bc.record('start',is_variable=True)
    phi.bc[0] = 0
    phi.bc.freeze()
    
    lapl = -n_dif(areac*c_dif(phi.symb))/area
    phi_solver = Solver1D(phi,lapl)
    #phi[:] = phi_solver.solve_full(rho)
    phi_solver.solve_full(phi,rho)
    #gop = lapl.arr/phi.symb.arr
    #op1 = sy.gen_op_total(gop)
    #op = sy.gen_op_simplify(gop)
    #print(op)
    print('phi(rho) =', phi)
    
    #%%
    phi.bc.update('start')
    phi.bc[0]=1
    #print('phi(rho) =', sy.gen_op_solve(lapl.arr/phi.symb.arr,rho.arr))
    #gop = lapl.arr/phi.symb.arr
    #op1 = sy.gen_op_total(gop)
    #op = sy.gen_op_simplify(gop)
    #print(op)
    phi_solver.solve_full(phi,rho)
    print('phi(rho) =', phi)


    #%% 2D field
    sy.SET_SPARSE(True)
    np.set_printoptions(threshold=1000)
    geomx = Geom1D(n_cells=50,delta=.02,start=0.0)
    geomy = Geom1D(n_cells=40,delta=.025,start=0.0)
    geoms=(geomx,geomy)
    rho = Field2D('rho',geoms,stags=(0,0),nls=(0,0),nus=(0,0))
    rho[:,:]=1 #np.ones(rho.nts)
    phi = Field2D('phi',geoms,stags=(0,0),nls=(1,1),nus=(1,1))
    #phi.symb[end+1,-1:end+1]=0 # gives an error
    #phi.arr[:,:]=np.random.rand(*phi.nts)
    phi.bc[end+1,-1:end+1]=0
    phi.bc[:,-1]=0
    phi.bc[:,end+1]=0
    phi.bc.record('left',is_variable=True)
    phi.bc[-1,-1:end+1]=0
    phi.bc.freeze()
    def Laplacian2D(f):
        return -n_dif(c_dif(f,0),0)-n_dif(c_dif(f,1),1)
    lapl = Laplacian2D(phi.symb)
    phi_solver = Solver2D(phi,lapl)
    phi_solver.solve_full(phi,rho) # bc are included
    #%%
    plt.figure()
    plt.pcolor(phi.pcolor_x(0),phi.pcolor_x(1),phi[:,:].T)
    #%%
#    phi.bc.update('left')
#    try:
#        phi.bc[0,:]=1
#        phi.bc.end_update()
#    except Exception as e:
#        print('Caught',repr(e))
    #%%
    phi.bc.update('left')
    phi.bc[-1,-1:end+1]=1
    phi_solver.solve_full(phi,rho)
    #%%
    plt.figure()
    plt.pcolor(phi.pcolor_x(0),phi.pcolor_x(1),phi.val.T)
    plt.colorbar()
    phi.bc.print()
    #plt.pcolor(phi.x(0),phi.x(1),phi.val.T)
