#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 22 23:49:58 2018

Rescue the situation when subregion get out of the boundary on top or bottom

Current limitations:
    Only works for stag=0 extended arrays!
    Periodic arrays assumed to have valid values at a[0:end-1]
    - we do not use values outside this interval; by periodicity a[end]==a[0] etc
    Only works for second index in 2D arrays!

@author: nle003
"""

import numpy as np
import numbers
from trump import Grid,Geometry1D,Geometry2D,ExtendedArray1D,ExtendedArray2D,end,span
from trump.extended_array import to_i, EndIndex
import scipy.interpolate

def inf_reslice(i,n):
    "Convert ExtendedArray slice to a Python array slice, without limits"
    if i is span:
        raise Exception('Cannot span the infinity')
    is_slice = isinstance(i,slice) 
    if is_slice:
        step = (1 if i.step is None else i.step)
        sign = (1 if step>0 else -1)
        if step == 0: raise ValueError('slice step is zero')
        # START
        if i.start is None:
            start = (0 if step>0 else n-1)
        else:
            start = to_i(i.start,n)
        # STOP
        if i.stop is None:
            stop = (n if step>0 else -1)
        else:
            stop = to_i(i.stop,n) + sign
        if (step>0 and start>=stop+step) or (step<0 and start<=stop+step):
            gt_or_lt = (' > ' if step>0 else ' < ')
            delta = '{:+d}'.format(-step)
            raise ValueError(('(start=={})'+delta+gt_or_lt+'(stop=={}), step=={}').format(
                             start,stop-sign,step))
        if stop == -1: # This is tricky, we cannot have -1
            stop = None
        i = slice(start,stop,i.step)
    elif isinstance(i,numbers.Integral) or isinstance(i,EndIndex):
        i = to_i(i,n)
    elif isinstance(i,np.ndarray):
        assert i.dtype is np.dtype('int')
        i = i.copy() # do not mess up the original
    else:
        raise IndexError('unimplemented index type')
    return i,is_slice

class InfiniteGrid:
    def __init__(self,grid,stag):
        self.d = grid.delta
        self.b = grid.start + (stag/2)*self.d
        self.n = grid.n_cells + 1 - stag
    def __getitem__(self,i):
        i,is_slice = inf_reslice(i,self.n)
        if is_slice:
            return self.b + np.arange(i.start,i.stop,i.step)*self.d
        else:
            return self.b + i*self.d
        
def grid_by_pyslice(grid,stag,s):
    "WARNING: s is a Python, not Trump slice!"
    assert (stag==0 or stag==1)
    d = grid.delta
    gstart = grid.start + (stag/2)*d
    return gstart + np.arange(s.start,s.stop,s.step)*d

def normalize_index(s,period,limit=True):
    """The index s which is outside the boundaries get normalized: either to an index or to
    a list of slices."""
    assert isinstance(s,slice) or isinstance(s,numbers.Integral) or isinstance(s,EndIndex)
    is_scalar = not isinstance(s,slice)
    if is_scalar:
        i = to_i(s,period+1)
        i = i % period
    else:
        assert s.step is None # Deal with more complicated later
        start = s.start
        stop = s.stop
        if start is None: start = 0
        if stop is None: stop = end # the default upper boundary, can be period or "end"
        assert isinstance(stop,numbers.Integral) or isinstance(stop,EndIndex)
        stop = to_i(stop,period+1)
        assert isinstance(start,numbers.Integral)
        if limit and stop-start+1>period:
            # When we set values, avoid setting more than one period
            raise Exception('Cannot set more than a period')
        # Shift to >= 0
        nper0 = start // period
        start -= nper0*period
        stop -= nper0*period
        i=[]
        while stop>period-1:
            i.append(slice(start,period-1,None))
            stop -= period
            start = 0
        i.append(slice(start,stop))
    return i,is_scalar

class PeriodicViewEA1D:
    """An object that allows periodic subindexing of an ExtendedArray1D
    without copying. Indexing is transparent."""
    def __init__(self,ea):
        assert ea.stag==0
        assert ea.nu >= -1
        assert ea.nl >= 0
        self.ea = ea
        self.period = ea.n-1
    def __getitem__(self,s):
        i,is_scalar = normalize_index(s,self.period,limit=False)
        if is_scalar:
            return self.ea[i]
        else:
            vlist = []
            for sl in i:
                vlist.append(self.ea[sl])
            return np.hstack(vlist)
    def __setitem__(self,s,v):
        assert isinstance(v,np.ndarray) or isinstance(v,numbers.Number)
        i,is_scalar = normalize_index(s,self.period,limit=True)
        if is_scalar:
            self.ea[i]=v
        else:
            vstart = 0
            for sl in i:
                nel = sl.stop-sl.start+1
                vstop = vstart+nel
                print(vstart,vstop)
                if isinstance(v,numbers.Number):
                    self.ea[sl]=v
                else:
                    self.ea[sl]=v[vstart:vstop]
                vstart = vstop
            if isinstance(v,np.ndarray):
                assert vstop==len(v)

class PeriodicViewEA2D:
    """An object that allows periodic subindexing of an ExtendedArray1D
    without copying. Indexing is transparent."""
    def __init__(self,ea,axis=1):
        self.ea = ea
        self.axis = axis
        self.period = ea.ns[axis]-1
        assert axis==1 # very limited implementation for now!!!
        assert ea.stags[axis]==0
        assert ea.nus[axis] >= -1
        assert ea.nls[axis] >= 0
    def __getitem__(self,ss):
        assert isinstance(ss,tuple)
        s0 = ss[0]
        i,is_scalar = normalize_index(ss[self.axis],self.period,limit=False)
        if is_scalar:
            return self.ea[s0,i]
        else:
            vlist = []
            for sl in i:
                vlist.append(self.ea[s0,sl])
            return np.hstack(vlist)
    def __setitem__(self,ss,v):
        assert isinstance(v,np.ndarray) or isinstance(v,numbers.Number)
        assert isinstance(ss,tuple)
        s0 = ss[0]
        i,is_scalar = normalize_index(ss[self.axis],self.period,limit=True)
        if is_scalar:
            self.ea[s0,i]=v
        else:
            vstart = 0
            for sl in i:
                nel = sl.stop-sl.start+1
                vstop = vstart+nel
                #print(vstart,vstop)
                if isinstance(v,numbers.Number):
                    self.ea[s0,sl]=v
                else:
                    if len(v.shape)==2:
                        self.ea[s0,sl]=v[:,vstart:vstop]
                    elif len(v.shape)==1:
                        self.ea[s0,sl]=v[vstart:vstop]
                    else:
                        raise Exception('wrong dimensions!')
                vstart = vstop
            if isinstance(v,np.ndarray):
                if len(v.shape)==1:
                    assert vstop==len(v)
                else:
                    assert vstop==v.shape[self.axis]

def periodic_ea2_interpolate(x,y,ea2_per,kind):
    "ea2 is PeriodicViewEA2D. Unfortunately, we cannot use its interface for this"
    # For debugging:
    assert ea2_per.axis==1
    xspan = ea2_per.ea.xe(0)
    yea=ea2_per.ea.x(1)
    need_periodic = np.max(y)>yea[-1] or np.min(y)<yea[0]
    if need_periodic:
        p = ea2_per.period
        grid = ea2_per.ea.geom.grids[ea2_per.axis]
        stag = ea2_per.ea.stags[ea2_per.axis]
        assert stag==0
        if np.max(y)>yea[-1]:
            # Add values on top
            #s = slice(0,2*ea2_per.period-1)
            yea_per = grid_by_pyslice(grid,stag,slice(0,2*p))
            tmp = scipy.interpolate.interp2d(yea_per, xspan, ea2_per[span,0:2*p-1],
                                      bounds_error=True, kind=kind)(y, x)
        elif np.min(y)<yea[0]:
            yea_per = grid_by_pyslice(grid,stag,slice(-p,p))
            tmp = scipy.interpolate.interp2d(yea_per, xspan, ea2_per[span,-p:p-1],
                                      bounds_error=True, kind=kind)(y, x)
    else:
        tmp = scipy.interpolate.interp2d(yea, xspan, ea2_per[span,:],
                                      bounds_error=True, kind=kind)(y, x)
    return tmp

#%%#################################################################################################
# Some testing
if __name__=='__main__':
    #%%
    import matplotlib.pyplot as plt
    from nleht_utils import header
    # Some basic testing
    grid = Grid(10,.1,0)
    geom = Geometry1D(grid)
    print(header('Test the inf grid'))
    yinf = InfiniteGrid(grid,0)
    #%%
    print(header('Test the 1D'))
    f = ExtendedArray1D(geom,stag=0,nl=0,nu=-1)
    f.arr = np.arange(10,dtype=np.double)
    #phi.alloc(np.double)
    f_per = PeriodicViewEA1D(f) # disregards phi[-1] and phi[end+1]
    print(f_per[0:end+4])
    print(f_per[:])
    f_per[-4:5]=np.arange(10)
    f_per[end+3]=np.asarray([100])
    f[:end-1]=np.sin(2*np.pi*yinf[:end-1])
    pp = f_per.period
    #plt.plot(yinf[0:pp*3],f_per[0:pp*3],'x-')
    #%%
    print(header('Test the 2D'))
    grid0 = Grid(5,.1,0) # silent
    #xinf = InfiniteGrid(grid0,stag=0)
    geom2 = Geometry2D(grid0,grid)
    p = ExtendedArray2D(geom2,stags=(0,0),nls=(0,0),nus=(0,-1))
    p.alloc(np.double)
    xe = p.ea_x(0)
    p.arr[:,:]=np.cos(4*np.pi*xe[span][:,np.newaxis])+np.sin(2*np.pi*yinf[:end-1][np.newaxis,:])
    p_per=PeriodicViewEA2D(p,axis=1)
    #print(p_per[1:2,8:end+5])
    p_per[1:2,8:end+5]=np.random.rand(2,8)
    p_per[3,-4:4]=np.random.randn(1,9)
    plt.figure()
    plt.pcolormesh(xe[span],yinf[-pp*3:pp*3],p_per[span,-pp*3:pp*3].T)
    #p_per[-1,-4:4]=np.random.randn(1,9)
    #%%
    print(header('Test 2D interpolation'))
    p.arr[:,:]=np.cos(4*np.pi*xe[span][:,np.newaxis])+np.sin(2*np.pi*yinf[:end-1][np.newaxis,:])
    plt.figure()
    plt.pcolormesh(xe[span],yinf[-pp*3:pp*3],p_per[span,-pp*3:pp*3].T)
    x = np.linspace(0.1,0.3,21)
    y = np.linspace(.5,1.5,21)
    tmp = periodic_ea2_interpolate(x,y,p_per,'linear')
    plt.figure()
    plt.pcolormesh(x,y,tmp.T)
    y = np.linspace(-.5,.5,21)
    tmp = periodic_ea2_interpolate(x,y,p_per,'linear')
    plt.figure()
    plt.pcolormesh(x,y,tmp.T)