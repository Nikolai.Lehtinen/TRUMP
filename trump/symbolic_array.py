#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 11:03:30 2016

Symbolic array which records a linear operator acting on it
The interface is:

from custom_numpy import customize, decustomize
a = SymbolicArray('a',N)
v = np.linspace(1,2,N-1)
b = customize(v)*(a[:-1]+a[1:])
a[1]=1
gop = b/a # generalized operator, including constraints
op1 = gen_op_total(gop) # Oper(M,V)
op2 = gen_op_simplify(gop) # a shortened version of Oper(M,V)

@author: nle003
"""

#%% Compulsory imports
import numpy as np
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg
import numbers
from collections import namedtuple
import trump
from trump.custom_ndarray import overload_for,customize,decustomize

# We are able to switch quickly between sparse and dense matrices
def SET_SPARSE(use_sparse=True):
    global USE_SPARSE,array_equal,argmax_row,eye,diag,vstack,solve,to_indexable,to_solvable,\
        to_dense,v_repeat,matrix
    USE_SPARSE=use_sparse
    if use_sparse:
        # Enhancements (!?) of the scipy.sparse package
        def array_equal(m1,m2):
            "I don't know why this is not in the package already"
            tmp = (m1 != m2)
            if isinstance(tmp,bool):
                assert tmp
                return False
            return (m1 != m2).nnz==0
        def argmax_row(m):
            "Returns a 1D ndarray"
            tmp = m.tolil()
            rows = tmp.rows
            data = tmp.data
            N = m.shape[0]
            inds = np.ndarray((N,),dtype=np.int) # preallocate
            for k in range(N):
                inds[k]=rows[k][np.argmax(data[k])]
            return inds
        eye=scipy.sparse.eye
        diag = scipy.sparse.diags
        vstack=scipy.sparse.vstack
        solve = scipy.sparse.linalg.spsolve
        def to_indexable(m): return m.tolil()
        def to_solvable(m): return m.tocsc()
        def to_dense(m): return m.A
        def v_repeat(m,n): return scipy.sparse.lil_matrix(np.repeat(m.A,n,axis=0))
        matrix = scipy.sparse.lil_matrix
    else:
        def array_equal(m1,m2):
            return np.array_equal(m1,m2)
        def argmax_row(m):
            return np.argmax(m,axis=1)
        def eye(n): return np.eye(n)
        diag = np.diag
        vstack=np.vstack
        solve = scipy.linalg.solve
        def to_indexable(m): return m
        def to_solvable(m): return m
        def to_dense(m): return m
        def v_repeat(m,n): return np.repeat(m,n,axis=0)
        #def matrix(s): return np.matrix(np.ndarray(s))
        matrix = np.ndarray

##%% Length of an index
def to_fancy_index(i,n):
    #print(np.repeat(np.arange(n)[i],1,axis=0))
    if True:
        return np.repeat(np.arange(n)[i],1,axis=0)
    else:
        # another way to do it
        tmp = np.zeros((n,),dtype=np.bool)
        tmp[i]=True
        return tmp

#%% Elementary operator M*x + V
# M is always a 2D np.ndarray (dense) or a sparse matrix (sparse)!
# V is always a 1D np.array!
Oper = namedtuple('Oper',['M','V'])

def oper_apply(op, x):
    "Returns M*x+V, where M, V are parts of the linear operator op == Oper(M,V)"
    return op.M.dot(x) + op.V   

def oper_solve(op, b):
    """Solves the linear equation set ``M * x + V = b`` for the unknown ``x``
    where M, V are parts of the linear operator op == Oper(M,V)"""
    return solve(to_solvable(op.M),b-op.V)
    
def oper_valid(op):
    return len(op.M.shape)==2 and len(op.V.shape)==1 and op.M.shape[0] == op.V.shape[0]

def oper_dense(op):
    return Oper(M=to_dense(op.M),V=op.V)

def oper_getitem(op,i):
    return Oper(M=to_indexable(op.M)[i,:],V=op.V[i])
    
def oper_cat(op1,op2):
    return Oper(M=vstack((op1.M,op2.M)),V=np.hstack((op1.V,op2.V)))

def oper_equal(op1,op2):
    "NOTE: no validity checking, use oper_valid"
    return (array_equal(op1.M,op2.M) and np.array_equal(op1.V,op2.V))

def oper_copy(op):
    return Oper(M=op.M.copy(),V=op.V.copy())

#%% Generalized operator, with constraints:
# Equation is eq.M * x + eq.V = rhs
# Constraints are cnst.M * x + cnst.V = 0
# dep is a bool mask showing which x components are dependent
GenOp = namedtuple('GenOp',['eq','cnst','dep'])

def gen_op_equal(gop1,gop2):
    "NOTE: no validity checking, use gen_op_valid"
    return oper_equal(gop1.eq,gop2.eq) and oper_equal(gop1.cnst,gop2.cnst) \
        and np.array_equal(gop1.dep,gop2.dep)
        
def gen_op_valid(gop):
    return oper_valid(gop.eq) and oper_valid(gop.cnst) and \
        gop.eq.M.shape[1]==gop.cnst.M.shape[1]==len(gop.dep) and \
        sum(gop.dep)==gop.cnst.M.shape[0]

def gen_op_apply(gen_op, x):
    "See also ConstrainedArray.apply_cnst(x)"
    eq, cnst, _ = gen_op
    res = eq.M.dot(x) + eq.V
    err = cnst.M.dot(x) + cnst.V
    return (res,err)
    
def gen_op_solve(gen_op, b):
    eq, cnst, _ = gen_op
    assert eq.M.shape[0]+cnst.M.shape[0]==eq.M.shape[1]
    assert len(b)==eq.M.shape[0]
    Vt = np.hstack((eq.V,cnst.V))
    bt = np.hstack((b,np.zeros((len(cnst.V),))))
    Mt = vstack((eq.M,cnst.M))
    return solve(to_solvable(Mt),bt-Vt)
    
def gen_op_total(gen_op):
    "Same as simplify, but without simplification"
    eq, cnst, _ = gen_op
    return oper_cat(eq,cnst)

def gen_op_simplify(gen_op):
    eq, cnst, dep = gen_op
    if any(dep):
        cM = to_indexable(cnst.M)
        eM = to_indexable(eq.M)
        A = solve(to_solvable(cM[:,dep].T),to_solvable(eM[:,dep].T)).T
        return Oper(M=eM[:,~dep]-A.dot(cM[:,~dep]),V=eq.V-A.dot(cnst.V))
    else:
        return eq

#%% A storage for constraints
class Constraint:
    def __init__(self,n):
        self.n = n
        self.op = Oper(M=np.ndarray((0,n)),V=np.ndarray((0,)))
        self.ind = np.ndarray((0,),dtype=np.int)
        self.dep = np.zeros((n,),dtype=np.bool)
    def append(self,op,ind=None):
        #print('Appending ',op,ind)
        assert op.M.shape[1]==self.n
        if ind is None:
            # Automatically determine ind
            M=op.M.copy()
            M[:,self.dep]=0
            ind = argmax_row(abs(M))
            for k in range(len(ind)):
                if M[k,ind[k]] == 0:
                    raise Exception('Conflicting dependencies')
        l = len(op.V)
        assert len(ind)==l
        #print(op.M,np.arange(l),ind)
        #print(op.M[(np.arange(l),ind)])
        if not to_dense(op.M[(np.arange(l),ind)]).all():
            raise Exception('ind='+str(ind)+' is given incorrectly')
        if self.dep[ind].any():
            raise Exception('Conflicting dependencies: these elements are already set')
        self.ind = np.hstack((self.ind,ind))
        self.op = oper_cat(self.op,op)
        self.dep[ind] = True
    def __repr__(self):
        "The most generic __repr__"
        return '%s(\n\t\t\t%s)' % (self.__class__.__name__, \
            ',\n\t\t\t'.join('%s=%s' % (k,repr(self.__dict__[k])) \
                                       for k in sorted(self.__dict__)))
    pass

#%% Constraints interface
class ConstrainedArray:
    """A wrapper for a linear system of equations, with parts of it being updateable
    A constraint is a system of sub-constraints, each sub-constraint is a submatrix
    in a common matrix.
    The sub-constraint equation starting numbers are in "addr".
    The constraints may change, the changeability is indicated by bool array "var"
    The total number of variables is "size", the dependent variables are indicated in
    bool array "dep"
    The variable constraints have to be introduced with "set_var_cnst" and updated
    with "update_var_cnst"
    """
    def __init__(self,name,n,array_like=False,debug=None):
        self._debug = trump.DEBUGLEVEL if debug is None else debug
        assert isinstance(name,str) and isinstance(n,numbers.Integral)
        assert n > 0
        self.name = name
        self.n = n
        self.array_like = array_like
        assert isinstance(n,numbers.Integral) and n > 0
        self.cnst = Constraint(n)
    def __repr__(self):
        "The most generic __repr__"
        return '%s(\n\t\t%s)' % (self.__class__.__name__, \
            ',\n\t\t'.join('%s=%s' % (k,repr(self.__dict__[k])) \
                                       for k in sorted(self.__dict__)))
    pass

#%% Fake array expression (any linear stuff)
class ConstraintManager:
    def workon(self,c,ind):
        pass

class DefaultConstraintManager(ConstraintManager):
    def __init__(self,cnst):
        self.cnst = cnst
    def workon(self,c,ind):
        self.cnst.append(c,ind)
    pass

class CAExpression:
    def __init__(self,ref,op=None,is_view=True):
        # Reference to the abstract fake array, dispatch by type
        assert isinstance(ref,ConstrainedArray)
        self.ref = ref
        self._debug = self.ref._debug
        n = self.ref.n # Will never change
        if op is None:
            self.op = Oper(M=to_indexable(eye(n)),V=np.zeros((n,)))
        else:
            if self._debug>0:
                assert len(op.M.shape)==2 and len(op.V.shape)==1
                assert op.M.shape[1]==n and op.M.shape[0]==len(op.V)
            self.op = op
        self.is_view = is_view
        self.cnst_manager = DefaultConstraintManager(ref.cnst)
    def from_MV(self,M,V,is_view=False):
        # Copy everything except M, V
        # Since it is a result of some operation, it is not a view
        return CAExpression(self.ref,op=Oper(M=M,V=V),is_view=is_view)
    def __getitem__(self,i):
        i = to_fancy_index(i,len(self)) # want i to be an array of indices
        return self.from_MV(self.op.M[i,:], self.op.V[i],
                            is_view=(self.is_view and isinstance(i,slice)))
    def __setitem__(self,i,rhs):
        "Set constraints"
        i = to_fancy_index(i,len(self)) # fancy index
        if self.ref.array_like:
            if not self.is_view:
                raise SyntaxError(
                'setting indeces for a non-view, not allowed for real arrays!')
            else:
                # Which indices of the original array?
                ind = argmax_row(self.op.M[i,:])
                # Check if it really a view
                if self._debug>1:
                    assert to_dense(self.op.M[(i,ind)]==1).all()
        else:
            # Cannot determine which variables are made dependent
            # Delegate this to Constraint.append
            ind = None
        # Calculate the constraint operator
        if isinstance(rhs,CAExpression):
            # A more complicated constraint
            if not (self.ref is rhs.ref):
                raise ValueError('Assigning '+rhs.ref.name+' to '+self.ref.name+' is not allowed')
            # Allow assigning single value to multiple values
            if rhs.op.M.shape[0]==1:
                tmp=v_repeat(rhs.op.M,len(i))
            else:
                tmp=rhs.op.M
            if not (tmp.shape==self.op.M[i,:].shape):
                raise ValueError('Assigning '+str(tmp.shape[0])+' values to '+\
                                 str(self.op.M[i,:].shape[0])+' not allowed')
            c = Oper(M=self.op.M[i,:]-tmp,V=self.op.V[i]-rhs.op.V)
        else:
            # Simple constraint
            # Make sure rhs is also an array
            #rhs = np.repeat(rhs,1,axis=0)
            # To make sure c.V is an ndarray with correct shape
            c = Oper(M=self.op.M[i,:],V=self.op.V[i]-rhs)
        #print('Constraint is',c,', ind=',ind)
        assert len(c.V) == c.M.shape[0] # sanity check
        self.cnst_manager.workon(c,ind)
    def __add__(self,fa):
        #print('add:\n',self,'\n + \n',fa)
        if isinstance(fa,CAExpression):
            assert self.ref is fa.ref
            # Also check dimentions
            assert len(self)==len(fa) or len(fa)==1 or len(self)==1
            if len(self)==len(fa):
                return self.from_MV(self.op.M+fa.op.M, self.op.V+fa.op.V)
            elif len(fa)==1:
                return self.from_MV(self.op.M+v_repeat(fa.op.M,len(self)),
                                    self.op.V+np.repeat(fa.op.V,len(self),axis=0))
            elif len(self)==1:
                return self.from_MV(v_repeat(self.op.M,len(fa))+fa.op.M,
                                    np.repeat(self.op.V,len(fa),axis=0)+fa.op.V)
            else:
                raise Exception('internal error')
        else:
            return self.from_MV(self.op.M, self.op.V+decustomize(fa))
    def __radd__(self,fa):
        return self + fa
    def __sub__(self,fa):
        return self + (-fa)
    def __rsub__(self,fa):
        return -self + fa
    def __neg__(self):
        return self.from_MV(-self.op.M,-self.op.V)
    def __pos__(self):
        return self
    def __mul__(self,v):
        if isinstance(v,numbers.Number):
            return self.from_MV(v*self.op.M,v*self.op.V)
        else:
            return self.from_MV(diag(decustomize(v)).dot(self.op.M),
                                decustomize(v)*self.op.V)
    def __rmul__(self,v):
        #print('----- rmul -----')
        return self*v
    def __truediv__(self,fa):
        if isinstance(fa,CAExpression):
            if not self.ref is fa.ref:
                raise SyntaxError('in (result/arg) the result must be from arg')
            if not (fa.is_view and array_equal(fa.op.M,eye(self.ref.n))):
                raise SyntaxError('in (result/arg) the arg must be the original array')
            #return np.solve(fa.M.T,self.M.T).T
            return self.gen_op()
        elif fa is None: # A shortcut since the ref is already known
            # Not to be used by user
            #return self/CAExpression(self.ref)
            return self.gen_op()
        else:
            # just an element-by-element division
            return self*(1/fa)
    def gen_op(self):
        "Same as self/None"
        gop=GenOp(eq=self.op,cnst=self.ref.cnst.op,dep=self.ref.cnst.dep)
        if self._debug>1: assert gen_op_valid(gop)
        return gop
    def __len__(self):
        "Different from self.ref.size in general because it could be a sub-array"
        return self.op.M.shape[0]
    def __repr__(self):
        "The most generic __repr__"
        return '%s(\n\t%s)' % (self.__class__.__name__, \
            ',\n\t'.join('%s=%s' % (k,repr(self.__dict__[k])) \
                                       for k in sorted(self.__dict__)))
    def copy(self):
        return CAExpression(self.ref.copy(),
                         op=Oper(M=self.op.M.copy(),V=self.op.V.copy()),
                         is_view=self.is_view)
    def __eq__(self,fa):
        if not isinstance(fa,CAExpression): return False
        return oper_equal(self.op,fa.op) and self.is_view == fa.is_view and \
            self.ref == fa.ref
    @property
    def flat(self):
        "It's always flat"
        return self
    pass

#%% A few more functions
# A small wrapper
def SymbolicArray(*a,**kw):
    return CAExpression(ConstrainedArray(*a,**kw))

#%% Preparations and defaults
overload_for(CAExpression)
SET_SPARSE()

#%%
if __name__=='__main__':
    #%%
    trump.DEBUGLEVEL=2
    SET_SPARSE(False)

    def get_dif(a):
        "User function, not part of package"
        return a[1:]-a[:-1]
    def get_lap(a):
        "User function, not part of package"
        return get_dif(get_dif(a))
    
    #%% Testing 1
    print('\n\n********** TEST 1 **********\n')
    f = SymbolicArray('f',5)
    ave = (f[1:] + f[:-1])/2
    dif = f[1:] - f[:-1]
    ave_op = ave/f
    dif_op = dif/f
    print('ave_op =',ave_op,'\n\ndif_op =',dif_op,'\n')
    
    #%% Testing 2
    print('\n\n********** TEST 2 **********\n')
    f = SymbolicArray('f',5)
    # A trick to make operations like v * f and v + f work with ndarray v and
    # arbitrary class f (NOTE: f * v and f + v work fine even without it)
    v = customize(np.arange(1,6))
    mul = v*f
    mul_op = mul/f
    print('mul_op =',mul_op,'\n')
    add = v + f
    add_op = add/f
    print('add_op =',add_op,'\n')
    sub1 = v - f
    sub1_op = sub1/f
    print('sub1_op =',sub1_op,'\n')
    sub2 = f - v
    sub2_op = sub2/f
    print('sub2_op =',sub2_op,'\n')
    div = f / v
    div_op = div/f
    print('div_op =',div_op,'\n')
    notweird = f * v[1]
    print('notweird =',notweird)
    tmp = v[1]
#    weird = tmp * f
#    print('weird =',weird)
    notweird2 = customize(tmp) * f
    print('notweird2 =',notweird2)
    
    #%% Testing 3
    print('\n\n********** TEST 3 **********\n')
    f = SymbolicArray('f',5)
    #f[:1]=np.array([1])
    #f[-1:]=np.array([-1])
    f[0]=1
    f[-1]=-1
    print(f.ref.cnst)
    lap = get_lap(f)
    lap_op = lap/f
    print('lap_op =',lap_op,'\n')
    rho = np.array([1,2,3])
    phi = gen_op_solve(lap_op,rho)
    print('phi =',phi)
    # introduce intentional error
    phi[0]=phi[0]+0.1
    print('phi_err =',phi)
    (rho,err) = gen_op_apply(lap_op,phi)
    print('rho =',rho,', err =',err)
    op = gen_op_simplify(lap_op)
    rho = oper_apply(op,phi[~lap_op.dep])
    print('Applied to indep components only: rho =',rho)
    
    #%% Testing 4
    print('\n\n********** TEST 4 **********\n')
    f = SymbolicArray('f',6)
    f[:2]=f[-2:]
    print(f.ref.cnst)
    lap = get_lap(f)
    lap_op = lap/f
    print('lap_op =',lap_op,'\n')
    phitest = np.array([1,2,3,4,1,2])
    rho,err = gen_op_apply(lap_op,phitest)
    print('\n\tphitest =',phitest,'\n\trho =',rho,'\n\terr=',err)
    phisol = gen_op_solve(lap_op,rho)
    (rhonew,errnew) = gen_op_apply(lap_op, phisol)
    print('\n\tphisol =',phisol,'\n\trhonew =',rhonew,'\n\terrnew=',errnew)
    
    #%% Testing 5
    for array_like in [False,True]:
        #
        print('\n\n********** TEST 5 **********\n')
        f = SymbolicArray('f',5,array_like=array_like)
        f[0]=f[-1:]
        # Constraint on the sum can be written as this:
        if array_like:
            f[1] = -f[0]-sum(f[2:]) + 1
        else:
            # incorrect syntax for real arrays
            sum(f)[:] = 1
        lap = get_dif(get_dif(f))
        lap_op = lap/f
        print('lap_op =',lap_op,'\n')
        rho=np.array([1,2,3])
        phi = gen_op_solve(lap_op,rho)
        print('\n\trho =',rho,'\n\tphi =',phi,'\n\tsum(phi) =',np.sum(phi))
        lap_s = gen_op_simplify(lap_op)
        phi = gen_op_solve(lap_op,rho)
        print('\n\trho =',rho,'\n\tphi =',phi,'\n\tsum(phi) =',np.sum(phi))
    
    #%% Testing 6
    print('\n\n********** TEST 6 **********\n')
    # The wrong way to set up dependeces. Still does for array_like=False
    f = SymbolicArray('f',5)
    g = f[0] + f[1]
    g[:] = 3.14
    (f[0] + f[2])[:] = 2.78
    # black magic
    gop = f/f
    op = gen_op_simplify(gop)
    print('The independent components are',
          [i for (i,j) in enumerate(~gop.dep) if j])
    rhs = np.array([1,2,3])
    sol = oper_apply(op,rhs)
    print('sol =',sol)
    print('Conditions check:')
    print('sol[0]+sol[1]=',sol[0]+sol[1],'(must be 3.14)')
    print(oper_apply(g.op,sol))
    print('sol[0]+sol[2]=',sol[0]+sol[2],'(must be 2.78)')
    h = f[0] + f[2]
    print(oper_apply(h.op,sol))
    
    #%% Same, the correct way
    print('\n\n********** TEST 6A **********\n')
    f = SymbolicArray('f',5,array_like=True)
    f[0] = 3.14 - f[1]
    f[2] = 2.78 - f[0]
    # black magic
    gop = f/f
    op = gen_op_simplify(gop)
    dep = gop.dep
    print('Indep:', [i for (i,j) in enumerate(dep) if not j],
          '; dep:', [i for (i,j) in enumerate(dep) if j])
    rhs = np.array([1,2,3])
    sol = oper_apply(op,rhs)
    print('sol =',sol)
    print('Conditions check:')
    print('sol[0]+sol[1]=',sol[0]+sol[1],'(must be 3.14)')
    print(oper_apply((f[0]+f[1]).op,sol))
    print('sol[0]+sol[2]=',sol[0]+sol[2],'(must be 2.78)')
    print(oper_apply((f[0]+f[2]).op,sol))
    
#    #%% The named constraints: requires a separate manager now
#    print('\n\n********** TEST 7 **********\n')
#    f = SymbolicArray('f',5,array_like=True)
#    f[-1]=1
#    kvc = f.ref.set_var_cnst()
#    f[0] = 3.14
#    f.ref.freeze_cnst()
#    f.ref.update_var_cnst(kvc)
#    f[0] = 2.78
#    # black magic
#    gop = f/f
#    assert gen_op_valid(gop)
#    op = gen_op_simplify(gop)
#    dep = gop.dep
#    print('Indep:', [i for (i,j) in enumerate(dep) if not j],
#          '; dep:', [i for (i,j) in enumerate(dep) if j])
#    rhs = np.array([1,2,3])
#    sol = oper_apply(op,rhs)
#    print('sol =',sol)
#    # another way to apply constraints
#    sol = np.random.rand(5)
#    f.ref.apply_cnst(sol)
#    print(sol)
#    f.ref.update_var_cnst(kvc)
#    f[0] = 18
#    f.ref.apply_cnst(sol)
#    print(sol)
    
    #%% Assigning and addition of single value to multiple indices
    print('\n\n********** TEST 8 **********\n')
    f = SymbolicArray('f',6,array_like=True)
    f[0:2]=2*f[2]-f[4:2:-1]
    print('Constraint is:\n',oper_dense(f.ref.cnst.op))
