#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 14:57:42 2016

Here is an example of how to use it (trump_demo.py):
    
import numpy as np
import matplotlib.pyplot as plt
import trump
from trump import end, Geom1D,  Field2D, Solver2D, c_dif, n_dif

#%%
trump.SET_SPARSE(True)
geomx = Geom1D(n_cells=50,delta=.02,start=0.0)
geomy = Geom1D(n_cells=40,delta=.025,start=0.0)
geoms=(geomx,geomy)
rho = Field2D('rho',geoms,stags=(0,0),nls=(-1,-1),nus=(-1,-1))
rho[1:end-1,1:end-1] = 1
phi = Field2D('phi',geoms,stags=(0,0),nls=(0,0),nus=(0,0))
phi.bc[end,:] = 0
phi.bc[1:end-1,0] = 0
phi.bc[1:end-1,end] = 0
phi.bc.record('left_edge',is_variable=True)
phi.bc[0,:] = 0
phi.bc.freeze()
def Laplacian2D(f):
    return n_dif(c_dif(f,0),0) + n_dif(c_dif(f,1),1)
lapl = Laplacian2D(phi.symb)
phi_solver = Solver2D(phi, -Laplacian2D(phi.symb))
#%% Plot #1
phi_solver.solve_full(phi,rho) # bc are automatically included
print(np.max(np.abs(Laplacian2D(phi)+rho).arr)) # Out: 5.00155472594e-13
xe = phi.pcolor_x(0); ye = phi.pcolor_x(1)
plt.figure()
plt.pcolor(xe,ye,phi.val.T)
plt.show()
#plt.savefig('trump_fig_1.png')
#%% Plot #2
phi.bc.update('left_edge')
phi.bc[0,:] = 1
phi.bc.end_update() # optional
phi_solver.solve_full(phi,rho)
print(np.max(np.abs(Laplacian2D(phi)+rho).arr)) # Out: 4.15134593368e-12
plt.figure()
plt.pcolor(xe,ye,phi.val.T)
plt.show()
#plt.savefig('trump_fig_2.png')

@author: Nikolai G. Lehtinen (Nikolai.Lehtinen@uib.no)
"""

#%% Preliminaries
print('Welcome to TRUMP (Total Removal of Unwanted Matrix Programming).')
import trump.symbolic_array
from trump.symbolic_array import SET_SPARSE
from trump.custom_ndarray import customize
import trump.extended_array
from trump.extended_array import grid_x, ExtendedArray1D, ExtendedArray2D, end, span, Grid,\
    Geometry1D, Geometry2D, \
    c_dif, n_dif, c_ave, n_ave, c_upc, n_upc, c_upn, n_upn, extend,\
    c_interp_upc, n_interp_upc, n_interp_upn, c_interp_upn
from trump.discrete_field import Field1D, Solver1D, Field2D, Solver2D

# Debug levels are:
# 0 - no error checking at all (DANGEROUS!)
# 1 - user input parameter error checking (recommended)
# 2 - internal error checking but no diagnostic printout
# 3 - some diagnostic printout
# 4 - detailed diagnostic output
DEBUGLEVEL = 1
SET_SPARSE(True)
print('=== Make coding great again! ===')
