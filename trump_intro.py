#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 13:00:45 2018

@author: nle003
"""

# Import the needed elements from TRUMP package
from trump import Grid, Geometry1D, Field1D, n_dif, c_dif, end, Solver1D
# Set up the calculation domain
N = 100 # number of cells
a = 0; b = 1 # the calculation domain [a,b]
dx = (b-a)/N # Delta x
grid = Grid(n_cells=N, delta=dx, start=a)
geom = Geometry1D(grid)
# Set up f
f = Field1D('f', geom, stag=0, nl=0, nu=0)
f.bc[0] = 1; f.bc[end] = 0; f.bc.freeze() # the BCs
f[1:end-1] = 0 # initial value at all points except boundaries
f.bc.apply() # enforce the explicit BCs
# Set up diffusion
D = 0.01
def Laplacian(fun):
    return n_dif(c_dif(fun))
dt = 0.01
implicit_diffusion = Solver1D(f,(f.symb - Laplacian(f.symb)*D*dt).view[1:end-1])
# Solve diffusion
T = 1
t = 0
while t <= T:
    # Solve with the implicit BCs
    implicit_diffusion.solve_full(f,f.view[1:end-1]) # calculate f in terms of f[1:end-1]
    t += dt
# Plot the results
import matplotlib.pyplot as plt
plt.plot(f.x,f[:])