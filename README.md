TRUMP (Total Removal of Unwanted Matrix Programming)
=======

# The newer version of the project is called FIDELIOR and is [here](https://gitlab.com/nleht/fidelior).

# Introduction

TRUMP (Total Removal of Unwanted Matrix Programming) is a package whose goal is
to help people with programming various finite-difference schemes. It allows the
user

1. to store the discretized values for physical variables in an object called
_Field_ which is defined on a 1D or 2D equally-spaced rectangular grid with
ghost cells at boundaries which are accessible with negative indices or indices
which look like `end+1` etc. The intermediate results for operations with fields
are stored in objects of class _ExtendedArray_ which have the same indexing features,
(but cannot generate operators or boundary conditions, see the rest of items).
2. to create finite-difference operators which act on Fields;
3. to specify boundary conditions on the Field which will be either
  * easily updated when using forward finite-differencing schemes
  * automatically enforced when inverting finite-difference operators acting on this field;
4. to set up a solver which will invert the finite-difference operator.

The purpose of the package is obviously to make finite-difference coding great again.

# Getting started: Running examples

Please use **Python3** (any version). Other requirements are listed in the [`requirements.txt`](requirements.txt) file.
We emphasize that the package does not provide any numerical solvers, it only acts as an interface
to solvers in Numpy. It is essential to have a good version of NumPy, because some of the previous versions were buggy.

## Read the manual
Read the [manual](trump.pdf) (at least, the introduction) and run [`trump_intro.py`](trump_intro.py)

## Operators, boundary conditions and solvers

You can start with running [`trump_demo.py`](trump_demo.py). The file is very short, please read what it does.

## Advection equation solution

Then, more complicated examples which demonstrate solution of advection equation

```math
\frac{\partial{f}}{\partial{t}} = -\nabla\cdot(\mathbf{v}f)
```

We use several different methods to solve it:

  * First order - CIR method
  * Second order - MacCormack method (a more stable modification of Lax-Wendroff)
  * Third order - Leonard method

The featured third-order method is of upwind type and also uses a flux corrector due to the same
author. To quote the author, "the fourth-order diffusion kills the third-order dispersion", therefore
the results are even better than if the fourth-order method was used.

The files are:

  * [`car_advection_test.py`](car_advection_test.py) - rotation in cartesian coordinates. It uses
    divergenceless velocity given by

    ```math
    \mathbf{v} = \omega(\hat{\mathbf{z}}\times\mathbf{r})
    ```

  * [`cyl_advection_test.py`](cyl_advection_test.py) - motion in cylindrical coordinates (there is an
    option for divergenceless velocity, too)
  * A more advanced example: [`cyl_advection_symmetry_test.py`](more_examples/cyl_advection_symmetry_test.py) - Here we test
    a situation for which the density changes uniformly, to make sure that
    behavior at the axis is correct.

# Other examples of _Markdown_ syntax

Gitlab uses [*GitLab Flavored Markdown (GFM)*](https://gitlab.com/help/user/markdown).
(There is another [copy of that manual](https://docs.gitlab.com/ee/user/markdown.html) which
is not rendered very well). [Cheatsheet](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf).
[For developers](https://github.github.com/gfm/) (don't read if you don't need problems).

Original flavor (?) [MarkDown syntax](https://daringfireball.net/projects/markdown/syntax) and
[cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

Images: ![Image](velocity.png)

Block comments: use `<!--` and `-->`:

<!--
All this should be 
commented out
-->

<!-- and this too -->

Another way: `[//] (comment)` (parentheses are **obligatory**!).

[//]: # (should be invisible)

Inline comments: use a trick of an empty link: `[](comment here)` [](comment here).


Math $`a^2+b^2=c^2`$

Two spaces at the end of a line  
leave a line break.

Horizontal rule:

---

> Markdown uses email-style > characters for blockquoting.

Inline <abbr title="Hypertext Markup Language">HTML</abbr> is supported.

This was ~~useless~~ useful [:smiley:](https://www.webpagefx.com/tools/emoji-cheat-sheet/)!

